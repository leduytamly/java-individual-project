/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

/**
 * Test Cases for Insurance Object
 * @author leduy
 */
public class InsuranceTest {
    
    /**
     * Test of getInsuranceName method, of class Insurance.
     */
    @Test
    public void testGetInsuranceName() {
        System.out.println("getInsuranceName");
        Insurance instance = new Insurance("La Capital",300);
        String expResult = "La Capital";
        String result = instance.getInsuranceName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInsuranceAmount method, of class Insurance.
     */
    @Test
    public void testGetInsuranceAmount() {
        System.out.println("getInsuranceAmount");
        Insurance instance = new Insurance("La Capital",300);
        double expResult = 300;
        double result = instance.getInsuranceAmount();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getPk method, of class Insurance.
     */
    @Test
    public void testGetPk() {
        System.out.println("getPk");
        Insurance instance = new Insurance(1,"La Capital",300);
        int expResult = 1;
        int result = instance.getPk();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInsuranceName method, of class Insurance.
     */
    @Test
    public void testSetInsuranceName() {
        System.out.println("setInsuranceName");
        String insuranceName = "BMO";
        Insurance instance = new Insurance("La Capital",300);
        instance.setInsuranceName(insuranceName);
    }

    /**
     * Test of setInsuranceAmount method, of class Insurance.
     */
    @Test
    public void testSetInsuranceAmount() {
        System.out.println("setInsuranceAmount");
        double insuranceAmount = 342.0;
        Insurance instance = new Insurance("La Capital",300);
        instance.setInsuranceAmount(insuranceAmount);
    }

    /**
     * Test of equals method, of class Insurance.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = new Insurance("La Capital",300);
        Insurance instance = new Insurance("La Capital",300);
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }
    
}
