/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.time.LocalDate;
import java.time.Month;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test cases for leases
 * @author leduy
 */
public class LeaseTest {

    /**
     * Test of getPk method, of class Lease.
     */
    @Test
    public void testGetPk() {
        System.out.println("getPk");
        Lease instance = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        int expResult = 1;
        int result = instance.getPk();
        assertEquals(expResult, result);
    }

    /**
     * Test of getStartdate method, of class Lease.
     */
    @Test
    public void testGetStartdate() {
        System.out.println("getStartdate");
        Lease instance = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        LocalDate expResult = LocalDate.of(2021, Month.APRIL, 30);
        LocalDate result = instance.getStartdate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEnddate method, of class Lease.
     */
    @Test
    public void testGetEnddate() {
        System.out.println("getEnddate");
        Lease instance = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        LocalDate expResult = LocalDate.of(2022, Month.APRIL, 30);
        LocalDate result = instance.getEnddate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSourcePath method, of class Lease.
     */
    @Test
    public void testGetSourcePath() {
        System.out.println("getSourcePath");
        Lease instance = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        String expResult = null;
        String result = instance.getSourcePath();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDestinationPath method, of class Lease.
     */
    @Test
    public void testGetDestinationPath() {
        System.out.println("getDestinationPath");
        Lease instance = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        String expResult = "C:\\Users\\leduy\\courses\\java410\\DestinationLease";
        String result = instance.getDestinationPath();
        assertEquals(expResult, result);
    }

    /**
     * Test of setStartdate method, of class Lease.
     */
    @Test
    public void testSetStartdate() {
        System.out.println("setStartdate");
        LocalDate startdate = LocalDate.of(2012, Month.JUNE, 2);
        Lease instance = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        instance.setStartdate(startdate);
    }

    /**
     * Test of setEnddate method, of class Lease.
     */
    @Test
    public void testSetEnddate() {
        System.out.println("setEnddate");
        LocalDate enddate = LocalDate.of(2012, Month.JUNE, 2);
        Lease instance = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        instance.setEnddate(enddate);
    }

    /**
     * Test of setSourcePath method, of class Lease.
     */
    @Test
    public void testSetSourcePath() {
        System.out.println("setSourcePath");
        String sourcePath = "source path";
         Lease instance = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        instance.setSourcePath(sourcePath);
    }

    /**
     * Test of setDestinationPath method, of class Lease.
     */
    @Test
    public void testSetDestinationPath() {
        System.out.println("setDestinationPath");
        String destinationPath = "destination path";
         Lease instance = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        instance.setDestinationPath(destinationPath);;
    }


    /**
     * Test of equals method, of class Lease.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Lease instance = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }
}
