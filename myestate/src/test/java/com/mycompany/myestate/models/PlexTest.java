/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test Case for Plex
 * @author leduy
 */
public class PlexTest {

    /**
     * Test of getPropertyName method, of class Property.
     */
    @Test
    public void testGetPk() {
        System.out.println("getPk");
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        int expResult = 1;
        int result = instance.getPk();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getPropertyName method, of class Property.
     */
    @Test
    public void testGetPropertyName() {
        System.out.println("getPropertyName");
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        String expResult = "Greenspace";
        String result = instance.getPropertyName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPropertyType method, of class Property.
     */
    @Test
    public void testGetPropertyType() {
        System.out.println("getPropertyType");
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        String expResult = "Plex";
        String result = instance.getPropertyType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPropertyAddress method, of class Property.
     */
    @Test
    public void testGetPropertyAddress() {
        System.out.println("getPropertyAddress");
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        String expResult = "123 street ,Montreal, Quebec";
        String result = instance.getPropertyAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPropertyCost method, of class Property.
     */
    @Test
    public void testGetPropertyCost() {
        System.out.println("getPropertyCost");
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        double expResult = 50000.00;
        double result = instance.getPropertyCost();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getPropertyDoors method, of class Property.
     */
    @Test
    public void testGetPropertyDoors() {
        System.out.println("getPropertyDoors");
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        int expResult = 6;
        int result = instance.getPropertyDoors();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPropertyRentalPrice method, of class Property.
     */
    @Test
    public void testGetPropertyRentalPrice() {
        System.out.println("getPropertyRentalPrice");
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        double expResult = 10.00;
        double result = instance.getPropertyRentalPrice();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getPropertyTaxRate method, of class Property.
     */
    @Test
    public void testGetPropertyTaxRate() {
        System.out.println("getPropertyTaxRate");
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        double expResult = 0.05;
        double result = instance.getPropertyTaxRate();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getPropertySchoolTaxRate method, of class Property.
     */
    @Test
    public void testGetPropertySchoolTaxRate() {
        System.out.println("getPropertySchoolTaxRate");
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        double expResult = 0.02;
        double result = instance.getPropertySchoolTaxRate();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getInsurance method, of class Property.
     */
    @Test
    public void testGetInsurance() {
        System.out.println("getInsurance");
        Insurance expResult = new Insurance(1,"intact",400);
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Insurance result = instance.getInsurance();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Property.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Plex instance = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        String expResult = "Greenspace ; 123 street ,Montreal, Quebec";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
