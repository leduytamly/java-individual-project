/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.time.LocalDate;
import java.time.Month;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test Cases for Tenant
 * @author leduy
 */
public class TenantTest {
    
    /**
     * Test of getPk method, of class Tenant.
     */
    @Test
    public void testGetPk() {
        System.out.println("getPk");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        int expResult = 1;
        int result = instance.getPk();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTenantFullName method, of class Tenant.
     */
    @Test
    public void testGetTenantFullName() {
        System.out.println("getTenantFullName");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        String expResult = "bob";
        String result = instance.getTenantFullName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getProperty method, of class Tenant.
     */
    @Test
    public void testGetProperty() {
        System.out.println("getProperty");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        Property expResult = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Property result = instance.getProperty();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTenantPhoneNumber method, of class Tenant.
     */
    @Test
    public void testGetTenantPhoneNumber() {
        System.out.println("getTenantPhoneNumber");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        String expResult = "514-232-2322";
        String result = instance.getTenantPhoneNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTenantEmail method, of class Tenant.
     */
    @Test
    public void testGetTenantEmail() {
        System.out.println("getTenantEmail");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        String expResult = "bob@email.com";
        String result = instance.getTenantEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTenantRentStart method, of class Tenant.
     */
    @Test
    public void testGetTenantRentStart() {
        System.out.println("getTenantRentStart");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        LocalDate expResult = LocalDate.of(2021, Month.APRIL, 30);
        LocalDate result = instance.getTenantRentStart();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTenantRentEnd method, of class Tenant.
     */
    @Test
    public void testGetTenantRentEnd() {
        System.out.println("getTenantRentEnd");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        LocalDate expResult = LocalDate.of(2021, Month.APRIL, 30);
        LocalDate result = instance.getTenantRentEnd();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTenantYearlyIncome method, of class Tenant.
     */
    @Test
    public void testGetTenantYearlyIncome() {
        System.out.println("getTenantYearlyIncome");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        double expResult = 40000;
        double result = instance.getTenantYearlyIncome();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getTenantIncomeDebtRatio method, of class Tenant.
     */
    @Test
    public void testGetTenantIncomeDebtRatio() {
        System.out.println("getTenantIncomeDebtRatio");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        double expResult = 1.2;
        double result = instance.getTenantIncomeDebtRatio();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getTenantStatus method, of class Tenant.
     */
    @Test
    public void testGetTenantStatus() {
        System.out.println("getTenantStatus");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        boolean expResult = false;
        boolean result = instance.getTenantStatus();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTenantPaymentMethond method, of class Tenant.
     */
    @Test
    public void testGetTenantPaymentMethond() {
        System.out.println("getTenantPaymentMethond");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        String expResult = "cheque";
        String result = instance.getTenantPaymentMethond();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLease method, of class Tenant.
     */
    @Test
    public void testGetLease() {
        System.out.println("getLease");
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        Lease expResult =  new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");;
        Lease result = instance.getLease();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTenantFullName method, of class Tenant.
     */
    @Test
    public void testSetTenantFullName() {
        System.out.println("setTenantFullName");
        
        String tenantFullName = "Chris Ross";
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setTenantFullName(tenantFullName);
    }

    /**
     * Test of setProperty method, of class Tenant.
     */
    @Test
    public void testSetProperty() {
        System.out.println("setProperty");
        
        Property property = new Plex(1, "SkyView","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));;
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setProperty(property);
    }

    /**
     * Test of setTenantPhoneNumber method, of class Tenant.
     */
    @Test
    public void testSetTenantPhoneNumber() {
        System.out.println("setTenantPhoneNumber");
        
        String tenantPhoneNumber = "323-234-3432";
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setTenantPhoneNumber(tenantPhoneNumber);
    }

    /**
     * Test of setTenantEmail method, of class Tenant.
     */
    @Test
    public void testSetTenantEmail() {
        System.out.println("setTenantEmail");
        String tenantEmail = "email@email.com";
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setTenantEmail(tenantEmail);
        
    }

    /**
     * Test of setTenantRentStart method, of class Tenant.
     */
    @Test
    public void testSetTenantRentStart() {
        System.out.println("setTenantRentStart");
        LocalDate tenantLeaseStart = LocalDate.of(2021, Month.APRIL, 30);
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setTenantRentStart(tenantLeaseStart);
    }

    /**
     * Test of setTenantRentEnd method, of class Tenant.
     */
    @Test
    public void testSetTenantRentEnd() {
        System.out.println("setTenantRentEnd");
        LocalDate tenantLeaseEnd = LocalDate.of(2022, Month.APRIL, 30);
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setTenantRentEnd(tenantLeaseEnd);
    }

    /**
     * Test of setTenantYearlyIncome method, of class Tenant.
     */
    @Test
    public void testSetTenantYearlyIncome() {
        System.out.println("setTenantYearlyIncome");
        double tenantYearlyIncome = 2200.0;
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setTenantYearlyIncome(tenantYearlyIncome);
    }

    /**
     * Test of setTenantIncomeDebtRation method, of class Tenant.
     */
    @Test
    public void testSetTenantIncomeDebtRation() {
        System.out.println("setTenantIncomeDebtRation");
        double tenantIncomeDebtRatio = 0.4;
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setTenantIncomeDebtRation(tenantIncomeDebtRatio);
    }

    /**
     * Test of setTenantStatus method, of class Tenant.
     */
    @Test
    public void testSetTenantStatus() {
        System.out.println("setTenantStatus");
        boolean tenantStatus = true;
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setTenantStatus(tenantStatus);
    }

    /**
     * Test of setTenantPaymentMethond method, of class Tenant.
     */
    @Test
    public void testSetTenantPaymentMethond() {
        System.out.println("setTenantPaymentMethond");
        String tenantPaymentMethond = "Cash";
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setTenantPaymentMethond(tenantPaymentMethond);
    }

    /**
     * Test of setLease method, of class Tenant.
     */
    @Test
    public void testSetLease() {
        System.out.println("setLease");
        Lease lease =  new Lease(2,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");;
        
        Lease l = new Lease(1,LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2022, Month.APRIL, 30),"C:\\Users\\leduy\\courses\\java410\\DestinationLease");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Tenant instance = new Tenant(1,"bob",p,"514-232-2322","bob@email.com",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2021, Month.APRIL, 30),40000,1.2,false,"cheque",l);
        
        instance.setLease(lease);
    }
}
