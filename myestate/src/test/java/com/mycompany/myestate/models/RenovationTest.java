/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test cases for renovation
 * @author leduy
 */
public class RenovationTest {

    /**
     * Test of getPk method, of class Renovation.
     */
    @Test
    public void testGetPk() {
        System.out.println("getPk");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        int expResult = 1;
        int result = instance.getPk();
        assertEquals(expResult, result);
    }

    /**
     * Test of getContractor method, of class Renovation.
     */
    @Test
    public void testGetContractor() {
        System.out.println("getContractor");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        Contractor expResult = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");;
        Contractor result = instance.getContractor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getProperty method, of class Renovation.
     */
    @Test
    public void testGetProperty() {
        System.out.println("getProperty");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        Property expResult = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Property result = instance.getProperty();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRenovationType method, of class Renovation.
     */
    @Test
    public void testGetRenovationType() {
        System.out.println("getRenovationType");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        String expResult = "Plumbing";
        String result = instance.getRenovationType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRenovationDescription method, of class Renovation.
     */
    @Test
    public void testGetRenovationDescription() {
        System.out.println("getRenovationDescription");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        String expResult = "Sink is leaking";
        String result = instance.getRenovationDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRenovationCost method, of class Renovation.
     */
    @Test
    public void testGetRenovationCost() {
        System.out.println("getRenovationCost");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        double expResult = 200;
        double result = instance.getRenovationCost();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getRenovationStatus method, of class Renovation.
     */
    @Test
    public void testGetRenovationStatus() {
        System.out.println("getRenovationStatus");
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        boolean expResult = true;
        boolean result = instance.getRenovationStatus();
        assertEquals(expResult, result);
    }

    /**
     * Test of setContractor method, of class Renovation.
     */
    @Test
    public void testSetContractor() {
        System.out.println("setContractor");
        
        Contractor contractor = new Contractor(1, "Helen", "Fix and Fix", "514-111-1111", "hellen@email.com","drywal");
        
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        instance.setContractor(contractor);
    }

    /**
     * Test of setProperty method, of class Renovation.
     */
    @Test
    public void testSetProperty() {
        System.out.println("setProperty");
        
        Property property = new Plex(1, "SkyView","123 street ,Brossard, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        instance.setProperty(property);
    }

    /**
     * Test of setRenovationType method, of class Renovation.
     */
    @Test
    public void testSetRenovationType() {
        System.out.println("setRenovationType");
        String renovationType = "Drywall";
        
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        instance.setRenovationType(renovationType);
    }

    /**
     * Test of setRenovationDescription method, of class Renovation.
     */
    @Test
    public void testSetRenovationDescription() {
        System.out.println("setRenovationDescription");
        String renovationDescription = "Hole in wall";
        
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        instance.setRenovationDescription(renovationDescription);
    }

    /**
     * Test of setRenovationCost method, of class Renovation.
     */
    @Test
    public void testSetRenovationCost() {
        System.out.println("setRenovationCost");
        double renovationCost = 4.3;
        
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        instance.setRenovationCost(renovationCost);
    }

    /**
     * Test of setRenovationStatus method, of class Renovation.
     */
    @Test
    public void testSetRenovationStatus() {
        System.out.println("setRenovationStatus");
        boolean isFinished = false;
        
        Property p = new Plex(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,6,10.00,0.05,0.02, new Insurance(1,"intact",400));
        Contractor c = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","paint");
        Renovation instance = new Renovation(1,c,p,"Plumbing","Sink is leaking",200,true);
        
        instance.setRenovationStatus(isFinished);
    }
}
