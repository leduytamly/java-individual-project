/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

/**
 * Test Case for Contractor
 * @author Le
 */
public class ContractorTest {
    
    /**
     * Test of getPk method, of class Contractor.
     */
    @Test
    public void testGetPk() {
        System.out.println("getPk");
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        int expResult = 1;
        int result = instance.getPk();
        assertEquals(expResult, result);
    }

    /**
     * Test of getContractorName method, of class Contractor.
     */
    @Test
    public void testGetContractorName() {
        System.out.println("getContractorName");
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        String expResult = "Bob";
        String result = instance.getContractorName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getContractorCompany method, of class Contractor.
     */
    @Test
    public void testGetContractorCompany() {
        System.out.println("getContractorCompany");
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        String expResult = "Builder";
        String result = instance.getContractorCompany();
        assertEquals(expResult, result);
    }

    /**
     * Test of getContractorPhoneNumber method, of class Contractor.
     */
    @Test
    public void testGetContractorPhoneNumber() {
        System.out.println("getContractorPhoneNumber");
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        String expResult = "514-111-1111";
        String result = instance.getContractorPhoneNumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of getContractorEmail method, of class Contractor.
     */
    @Test
    public void testGetContractorEmail() {
        System.out.println("getContractorEmail");
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        String expResult = "bob@email.com";
        String result = instance.getContractorEmail();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getContractorEmail method, of class Contractor.
     */
    @Test
    public void testGetContractorProfession() {
        System.out.println("getContractorProfession");
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        String expResult = "Paint";
        String result = instance.getContractorProfession();
        assertEquals(expResult, result);
    }

    /**
     * Test of setContractorName method, of class Contractor.
     */
    @Test
    public void testSetContractorName() {
        System.out.println("setContractorName");
        String contractorName = "bob";
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        instance.setContractorName(contractorName);
    }

    /**
     * Test of setContractorCompany method, of class Contractor.
     */
    @Test
    public void testSetContractorCompany() {
        System.out.println("setContractorCompany");
        String contractorCompany = "compnay";
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        instance.setContractorCompany(contractorCompany);
    }

    /**
     * Test of setContractorPhoneNumber method, of class Contractor.
     */
    @Test
    public void testSetContractorPhoneNumber() {
        System.out.println("setContractorPhoneNumber");
        String contractorPhoneNumber = "322-322-3433";
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        instance.setContractorPhoneNumber(contractorPhoneNumber);
    }

    /**
     * Test of setContractorEmail method, of class Contractor.
     */
    @Test
    public void testSetContractorEmail() {
        System.out.println("setContractorEmail");
        String contractorEmail = "email@email.com   ";
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        instance.setContractorEmail(contractorEmail);
    }

    /**
     * Test of setContractorProfession method, of class Contractor.
     */
    @Test
    public void testSetContractorProfession() {
        System.out.println("setContractorProfession");
        String contractorProfession = "Plumbing";
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        instance.setContractorProfession(contractorProfession);
    }

    /**
     * Test of toString method, of class Contractor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        String expResult = "Bob ; Builder";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Contractor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        Contractor instance = new Contractor(1, "Bob", "Builder", "514-111-1111", "bob@email.com","Paint");
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }
}
