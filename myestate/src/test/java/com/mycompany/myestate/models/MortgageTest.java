/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.time.LocalDate;
import java.time.Month;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test Cases for Mortgage
 * @author leduy
 */
public class MortgageTest {

    /**
     * Test of getPropertyName method, of class Mortgage.
     */
    @Test
    public void testGetPropertyName() {
        System.out.println("getPropertyName");
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        String expResult = "Greenspace";
        String result = instance.getPropertyName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBankName method, of class Mortgage.
     */
    @Test
    public void testGetBankName() {
        System.out.println("getBankName");
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        BankInstitution expResult = new BankInstitution(1,"RBC",2.4);
        BankInstitution result = instance.getBank();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNextPaymentDate method, of class Mortgage.
     */
    @Test
    public void testGetNextPaymentDate() {
        System.out.println("getNextPaymentDate");
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        LocalDate expResult = LocalDate.of(2021, Month.APRIL, 30);
        LocalDate result = instance.getNextPaymentDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getStartDate method, of class Mortgage.
     */
    @Test
    public void testGetStartDate() {
        System.out.println("getStartDate");
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        LocalDate expResult = LocalDate.of(2020, Month.APRIL, 30);
        LocalDate result = instance.getStartDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEndDate method, of class Mortgage.
     */
    @Test
    public void testGetEndDate() {
        System.out.println("getEndDate");
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        LocalDate expResult = LocalDate.of(2025, Month.APRIL, 30);
        LocalDate result = instance.getEndDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDownPayment method, of class Mortgage.
     */
    @Test
    public void testGetDownPayment() {
        System.out.println("getDownPayment");
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        double expResult = 5000;
        double result = instance.getDownPayment();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getPk method, of class Mortgage.
     */
    @Test
    public void testGetPk() {
        System.out.println("getPk");
        Mortgage instance = new Mortgage(1,"Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        int expResult = 1;
        int result = instance.getPk();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBank method, of class Mortgage.
     */
    @Test
    public void testGetBank() {
        System.out.println("getBank");
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        BankInstitution expResult = new BankInstitution(1,"RBC",2.4);
        BankInstitution result = instance.getBank();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPropertyName method, of class Mortgage.
     */
    @Test
    public void testSetPropertyName() {
        System.out.println("setPropertyName");
        String propertyName = "Property";
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        instance.setPropertyName(propertyName);
    }

    /**
     * Test of setNextPaymentDate method, of class Mortgage.
     */
    @Test
    public void testSetNextPaymentDate() {
        System.out.println("setNextPaymentDate");
        LocalDate nextPaymentDate = LocalDate.of(2021, Month.APRIL, 30);
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        instance.setNextPaymentDate(nextPaymentDate);
    }

    /**
     * Test of setStartDate method, of class Mortgage.
     */
    @Test
    public void testSetStartDate() {
        System.out.println("setStartDate");
        LocalDate startDate = LocalDate.of(2021, Month.APRIL, 30);
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        instance.setStartDate(startDate);
    }

    /**
     * Test of setEndDate method, of class Mortgage.
     */
    @Test
    public void testSetEndDate() {
        System.out.println("setEndDate");
        LocalDate endDate = LocalDate.of(2021, Month.APRIL, 30);
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        instance.setEndDate(endDate);
    }

    /**
     * Test of setDownPayment method, of class Mortgage.
     */
    @Test
    public void testSetDownPayment() {
        System.out.println("setDownPayment");
        double downPayment = 2.1;
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        instance.setDownPayment(downPayment);
    }

    /**
     * Test of setBank method, of class Mortgage.
     */
    @Test
    public void testSetBank() {
        System.out.println("setBank");
        BankInstitution bank = new BankInstitution(1,"RBC",2.4);
        Mortgage instance = new Mortgage("Greenspace",LocalDate.of(2021, Month.APRIL, 30),LocalDate.of(2020, Month.APRIL, 30),LocalDate.of(2025, Month.APRIL, 30),5000,new BankInstitution(1,"RBC",2.4));
        instance.setBank(bank);
    }
    
}
