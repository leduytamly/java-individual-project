/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test Cases for Bank Institution
 * @author leduy
 */
public class BankInstitutionTest {  

    /**
     * Test of getInstitutionName method, of class BankInstitution.
     */
    @Test
    public void testGetInstitutionName() {
        System.out.println("getInstitutionName");
        BankInstitution instance = new BankInstitution("RBC",2.3); 
        String expResult = "RBC";
        String result = instance.getInstitutionName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInterestRate method, of class BankInstitution.
     */
    @Test
    public void testGetInterestRate() {
        System.out.println("getInterestRate");
        BankInstitution instance = new BankInstitution("RBC",2.3); 
        double expResult = 2.3;
        double result = instance.getInterestRate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInstitutionName method, of class BankInstitution.
     */
    @Test
    public void testSetInstitutionName() {
        System.out.println("setInstitutionName");
        BankInstitution instance = new BankInstitution("RBC",2.3); 
        String institutionName = "hello";
        instance.setInstitutionName(institutionName);
    }

    /**
     * Test of setInterestRate method, of class BankInstitution.
     */
    @Test
    public void testSetInterestRate() {
        System.out.println("setInterestRate");
        BankInstitution instance = new BankInstitution("RBC",2.3); 
        Double rate = 2.0;
        instance.setInterestRate(rate);
    }

    /**
     * Test of toString method, of class BankInstitution.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        BankInstitution instance = new BankInstitution("RBC",2.3); 
        String expResult = "RBC: 2.3%";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class BankInstitution.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        BankInstitution instance = new BankInstitution("RBC",2.3); 
        Object other = new BankInstitution("RBC",2.3);
        boolean expResult = true;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
    }
}
