/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * TestCases for Condo
 * @author leduy
 */
public class CondoTest {

    
    /**
     * Test of getPropertyName method, of class Property.
     */
    @Test
    public void testGetPk() {
        System.out.println("getPk");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        int expResult = 1;
        int result = instance.getPk();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getPropertyName method, of class Property.
     */
    @Test
    public void testGetPropertyName() {
        System.out.println("getPropertyName");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        String expResult = "Greenspace";
        String result = instance.getPropertyName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPropertyType method, of class Property.
     */
    @Test
    public void testGetPropertyType() {
        System.out.println("getPropertyType");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        String expResult = "Condo";
        String result = instance.getPropertyType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPropertyAddress method, of class Property.
     */
    @Test
    public void testGetPropertyAddress() {
        System.out.println("getPropertyAddress");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        String expResult = "123 street ,Montreal, Quebec";
        String result = instance.getPropertyAddress();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPropertyCost method, of class Property.
     */
    @Test
    public void testGetPropertyCost() {
        System.out.println("getPropertyCost");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        double expResult = 50000.00;
        double result = instance.getPropertyCost();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getPropertyDoors method, of class Property.
     */
    @Test
    public void testGetPropertyDoors() {
        System.out.println("getPropertyDoors");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        int expResult = 1;
        int result = instance.getPropertyDoors();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPropertyRentalPrice method, of class Property.
     */
    @Test
    public void testGetPropertyRentalPrice() {
        System.out.println("getPropertyRentalPrice");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        double expResult = 10.00;
        double result = instance.getPropertyRentalPrice();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getPropertyTaxRate method, of class Property.
     */
    @Test
    public void testGetPropertyTaxRate() {
        System.out.println("getPropertyTaxRate");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        double expResult = 0.05;
        double result = instance.getPropertyTaxRate();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getPropertySchoolTaxRate method, of class Property.
     */
    @Test
    public void testGetPropertySchoolTaxRate() {
        System.out.println("getPropertySchoolTaxRate");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        double expResult = 0.02;
        double result = instance.getPropertySchoolTaxRate();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getInsurance method, of class Property.
     */
    @Test
    public void testGetInsurance() {
        System.out.println("getInsurance");
        Insurance expResult = new Insurance(1,"intact",400);
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        Insurance result = instance.getInsurance();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Property.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        String expResult = "Greenspace ; 123 street ,Montreal, Quebec";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCondoFee method, of class Condo.
     */
    @Test
    public void testGetCondoFee() {
        System.out.println("getCondoFee");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        double expResult = 233;
        double result = instance.getCondoFee();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCondoFee method, of class Condo.
     */
    @Test
    public void testSetCondoFee() {
        System.out.println("setCondoFee");
        Condo instance = new Condo(1, "Greenspace","123 street ,Montreal, Quebec",50000.00,10.00,0.05,0.02, new Insurance(1,"intact",400), 233);
        double condoFee = 2.0;
        instance.setCondoFee(condoFee);
    }
}
