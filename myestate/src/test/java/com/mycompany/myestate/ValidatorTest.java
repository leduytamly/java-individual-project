/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test cases for Validator Class
 * @author leduy
 */
public class ValidatorTest {

    /**
     * Test of isDecimalNumber method, of class Validator.
     */
    @Test
    public void testIsDecimalNumber() {
        System.out.println("isDecimalNumber");
        String s = "123";
        boolean expResult = true;
        boolean result = Validator.isDecimalNumber(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isDecimalNumber method, of class Validator.
     * Case where there is a trailing period
     */
    @Test
    public void testIsDecimalNumberTrailingPeriod() {
        System.out.println("isDecimalNumber with trailing period");
        String s = "123.";
        boolean expResult = true;
        boolean result = Validator.isDecimalNumber(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isDecimalNumber method, of class Validator.
     * Case where there are decimal numbers
     */
    @Test
    public void testIsDecimalNumberAfterPeriod() {
        System.out.println("isDecimalNumber with numbers after period");
        String s = "123.23";
        boolean expResult = true;
        boolean result = Validator.isDecimalNumber(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isDecimalNumber method, of class Validator.
     * Case where there are letters and symbols
     */
    @Test
    public void testIsDecimalNumberNonNumericalCharacters() {
        System.out.println("isDecimalNumber with letters and symbols");
        String s = "ewrij,<";
        boolean expResult = false;
        boolean result = Validator.isDecimalNumber(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isDecimalNumber method, of class Validator.
     * Case where there are mixed decimal, characters, and symbols
     */
    @Test
    public void testIsDecimalNumberMixedCharactorTypes() {
        System.out.println("isDecimalNumber with mixed characters");
        String s = "wi234ij,";
        boolean expResult = false;
        boolean result = Validator.isDecimalNumber(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidEmail method, of class Validator.
     */
    @Test
    public void testIsValidEmail() {
        System.out.println("isValidEmail");
        String s = "hello@email.com";
        boolean expResult = true;
        boolean result = Validator.isValidEmail(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidEmail method, of class Validator.
     */
    @Test
    public void testIsValidEmailNoAt() {
        System.out.println("isValidEmail with no @ symbol");
        String s = "hello";
        boolean expResult = false;
        boolean result = Validator.isValidEmail(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidEmail method, of class Validator.
     */
    @Test
    public void testIsValidEmailNothingAfterAt() {
        System.out.println("isValidEmail with nothing after @");
        String s = "hello@";
        boolean expResult = false;
        boolean result = Validator.isValidEmail(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidEmail method, of class Validator.
     */
    @Test
    public void testIsValidEmailNoTLD() {
        System.out.println("isValidEmail with no tld");
        String s = "hello@email";
        boolean expResult = false;
        boolean result = Validator.isValidEmail(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidEmail method, of class Validator.
     */
    @Test
    public void testIsValidEmailNoTLDTrailingPeriod() {
        System.out.println("isValidEmail with period but no tld");
        String s = "hello@email.";
        boolean expResult = false;
        boolean result = Validator.isValidEmail(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidEmail method, of class Validator.
     */
    @Test
    public void testIsValidEmailOneCharTLD() {
        System.out.println("isValidEmail with tld which is 1 character");
        String s = "hello@email.q";
        boolean expResult = false;
        boolean result = Validator.isValidEmail(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidEmail method, of class Validator.
     */
    @Test
    public void testIsValidEmailTwoCharTLD() {
        System.out.println("isValidEmail two letter tld");
        String s = "hello@email.qc";
        boolean expResult = true;
        boolean result = Validator.isValidEmail(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidEmail method, of class Validator.
     */
    @Test
    public void testIsValidEmailMoreDomains() {
        System.out.println("isValidEmail more than 1 tld");
        String s = "hello@email.qc.ca";
        boolean expResult = true;
        boolean result = Validator.isValidEmail(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of isValidPhoneNumber method, of class Validator.
     */
    @Test
    public void testIsValidPhoneNumberWithDashes() {
        System.out.println("isValidPhoneNumber with dashes");
        String s = "230-232-2323";
        boolean expResult = true;
        boolean result = Validator.isValidPhoneNumber(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidPhoneNumber method, of class Validator.
     */
    @Test
    public void testIsValidPhoneNumberWithParensAndDash() {
        System.out.println("isValidPhoneNumber with parenthesis and dashes");
        String s = "(231) 232-2323";
        boolean expResult = true;
        boolean result = Validator.isValidPhoneNumber(s);
        assertEquals(expResult, result);
    }
    
        
    /**
     * Test of isValidPhoneNumber method, of class Validator.
     */
    @Test
    public void testIsValidPhoneNumberWithLetters() {
        System.out.println("isValidPhoneNumber with letters");
        String s = "(231) 2e2-2s22";
        boolean expResult = false;
        boolean result = Validator.isValidPhoneNumber(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidPhoneNumber method, of class Validator.
     */
    @Test
    public void testIsValidPhoneNumberMissingNumbersFirstSet() {
        System.out.println("isValidPhoneNumber missing numbers first set");
        String s = "(231) 22-2222";
        boolean expResult = false;
        boolean result = Validator.isValidPhoneNumber(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidPhoneNumber method, of class Validator.
     */
    @Test
    public void testIsValidPhoneNumberMissingNumbersSecondSet() {
        System.out.println("isValidPhoneNumber missing numbers second set");
        String s = "(231) 222-222";
        boolean expResult = false;
        boolean result = Validator.isValidPhoneNumber(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidPhoneNumber method, of class Validator.
     */
    @Test
    public void testIsValidPhoneNumberJustNumbers() {
        System.out.println("isValidPhoneNumber with just numbers");
        String s = "2312222222";
        boolean expResult = false;
        boolean result = Validator.isValidPhoneNumber(s);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isValidPhoneNumber method, of class Validator.
     */
    @Test
    public void testIsValidPhoneNumberWithSpaces() {
        System.out.println("isValidPhoneNumber with just spaces");
        String s = "231 222 2222";
        boolean expResult = true;
        boolean result = Validator.isValidPhoneNumber(s);
        assertEquals(expResult, result);
    }
}
