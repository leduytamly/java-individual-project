module com.mycompany.myestate {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.desktop;

    opens com.mycompany.myestate to javafx.fxml,java.sql, java.desktop;
    opens com.mycompany.myestate.models to javafx.fxml,java.sql, java.desktop;
    exports com.mycompany.myestate;
    exports com.mycompany.myestate.models;
}
