/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Contractor;
import com.mycompany.myestate.models.Property;
import com.mycompany.myestate.models.Renovation;
import com.mycompany.myestate.models.RenovationManager;
import java.io.IOException;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *Controller Class for Edit Renovation
 * Can also be set to read-only to be viewed 
 * @author leduy
 */
public class EditRenovationController extends ARenovationController{
    
    /*Renovation Object*/
    private Renovation selectedRenovation;
    
    /*FXML id for the header*/
    @FXML private Label header;
    /*Confirm Button*/
    @FXML private Button confirmBtn;
    
    @FXML
    private void initialize() throws SQLException{
        super.setup();
        super.setAllListeners();
    }
    
    /**
     * Initializes Edit Renovation view with the inputs pre loaded
     * @param renovation Renovation object to pre load
     */
    public void initializeRenovation(Renovation renovation) {
        selectedRenovation = renovation;
        property.setValue(selectedRenovation.getProperty().getPropertyName());
        contractor.setValue(selectedRenovation.getContractor().getContractorName());
        renovationType.setText(selectedRenovation.getRenovationType());
        renovationCost.setText(Double.toString(selectedRenovation.getRenovationCost()));
        renovationDescription.setText(selectedRenovation.getRenovationDescription());
        renovationStatus.setValue(translateStatusChoiceBox(selectedRenovation.getRenovationStatus()));
        property.setValue(renovation.getProperty());
        contractor.setValue(renovation.getContractor());
    }
    
    /**
     * Disables all inputs for the purpose of viewing only
     */
    public void setReadOnly(){
        /*Changes the header*/
        header.setText("Overview of Renovation");
        
        /*Disables all inputs*/
        property.setDisable(true);
        contractor.setDisable(true);
        renovationType.setDisable(true);
        renovationCost.setDisable(true);
        renovationDescription.setDisable(true);
        renovationStatus.setDisable(true);
        confirmBtn.setVisible(false);
    }
    
    /**
     * Sets the renovation with new values
     */
    private void setRenovation(){
        selectedRenovation.setContractor((Contractor)contractor.getValue());
        selectedRenovation.setProperty((Property)property.getValue());
        selectedRenovation.setRenovationType(renovationType.getText());
        selectedRenovation.setRenovationDescription(renovationDescription.getText());
        selectedRenovation.setRenovationCost(Double.parseDouble(renovationCost.getText()));
        selectedRenovation.setRenovationStatus(translateStatusChoiceBox());
    }
    
     /**
     * Confirm the changes to a renovation
     * @throws IOException
     */
    @FXML
    private void makeChange(ActionEvent event) throws IOException{
        if (switchConfirmationBox("Are you sure you want to make the change?")){
            try {
                setRenovation();
                /*Data validation*/
                super.validateAllFields();
                
                RenovationManager renovationManager = new RenovationManager();
                renovationManager.updateRenovation(selectedRenovation);
                super.switchToRenovations(event);
            } catch (IOException e) {
                System.out.println(e);
                System.out.println("ERROR SHOWING WARNING");
            } catch(IllegalArgumentException e) {
                System.out.println(e);
                switchWarningBox("Missing values!");
            }
        }
    }

}
