/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.ContractorManager;
import com.mycompany.myestate.models.PropertyManager;
import java.sql.SQLException;
import java.util.HashSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Abstract class for New renovation and Edit renovation
 * @author leduy
 */
public class ARenovationController extends AController {
    /*FXML ids for the inputs*/
    @FXML protected ChoiceBox property;
    @FXML protected ChoiceBox contractor;
    @FXML protected TextField renovationType;
    @FXML protected TextField renovationCost;
    @FXML protected TextArea renovationDescription;
    @FXML protected ChoiceBox renovationStatus;
    
    /**
     * Sets up all the choice boxes
     */
    protected void setup() {
        initializePropertyChoiceBox();
        initializeContractorChoiceBox();
        initializeStatusChoiceBox();
    }
    
    /**
     * Sets all the listeners
     */
    protected void setAllListeners(){
        setListeneresNumberField();
    }
    
    /**
     * validates all fields
     */
    protected void validateAllFields(){
        validateEmptyTextField();
        validateEmptyTextArea();
        validateEmptyChoiceBox();
        
        super.isValid();
    }
    
    /**
     * Initialize property choice box
     * @throws SQLException 
     */
    private void initializePropertyChoiceBox() {
        PropertyManager pm = new PropertyManager();
        property.setItems(pm.loadAllProperties());
    }
    
    /**
     * Initialize contractor choice box
     * @throws SQLException 
     */
    private void initializeContractorChoiceBox() {
        ContractorManager cm = new ContractorManager();
        contractor.setItems(cm.loadAllContractors());
    }
    
    /**
     * Initialize status choice box
     */
    private void initializeStatusChoiceBox(){
        ObservableList<String> options = FXCollections.observableArrayList("Finished","Unfinished");
        renovationStatus.setItems(options);
    }
    
    /**
     * Sets event listeners for numerical fields
     */
    private void setListeneresNumberField() {
        HashSet<TextField> numberFields = new HashSet<>();
        numberFields.add(renovationCost);
        super.validateNumbers(numberFields);
    }
    
    /**
     * Throws an exception if a non-numerical TextField is empty
     */
    private void validateEmptyTextField() {
        HashSet<TextField> textFields = new HashSet<>();
        textFields.add(renovationType);
        textFields.add(renovationCost);
        
        super.validateEmptyTextField(textFields);
    }
    
    /**
     * Throws an exception if a non-numerical TextArea is empty
     */
    private void validateEmptyTextArea() {
        HashSet<TextArea> textAreas = new HashSet<>();
        textAreas.add(renovationDescription);
        
        super.validateEmptyTextArea(textAreas);
    }
    
    /**
     * Validates empty choice box
     */
    private void validateEmptyChoiceBox(){
        HashSet<ChoiceBox> choiceBoxes = new HashSet<>();
        choiceBoxes.add(property);
        choiceBoxes.add(contractor);
        choiceBoxes.add(renovationStatus);
        
        super.validateEmptyChoiceBox(choiceBoxes);
    }
    
    /**
     * Translates the choice box input into Boolean
     * @return Finished is true, Unfinished is false
     */
    protected boolean translateStatusChoiceBox(){
        return ((String)renovationStatus.getValue()).equals("Finished");
    }
    
    /**
     * Translate a Boolean into a choice box input
     * @param b true is finished, false is unfinished
     * @return String for the choice box
     */
    protected String translateStatusChoiceBox(Boolean b){
        System.out.println(b);
        if (b){
            return "Finished";
        }
        return "Unfinished";
    }
}
