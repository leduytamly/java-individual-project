/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Contractor;
import com.mycompany.myestate.models.Property;
import com.mycompany.myestate.models.Renovation;
import com.mycompany.myestate.models.RenovationManager;
import java.io.IOException;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *Controller Class for Renovation View
 * @author leduy
 */
public class RenovationController extends AController{
    
    /*FXML ids for the table and columns*/
    @FXML private TableView<Renovation> tableView;
    @FXML private TableColumn<Renovation,Contractor> contractorNameCol;
    @FXML private TableColumn<Renovation,Property> propertyNameCol;
    @FXML private TableColumn<Renovation,String> renovationTypeCol;
    
    /**
     * Switch the scene to New Renovation View
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToNewRenovation(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("newrenovation");
        Scene newRenovationScene = new Scene(loader.load());
        showScene(event,newRenovationScene);
    }
    
    /**
     * Deletes a renovation, Displays a confirmation box before
     * @throws IOException 
     */
    public void deleteRenovation() throws IOException {
        RenovationManager rm = new RenovationManager();
        try{
            Renovation renovation = tableView.getSelectionModel().getSelectedItem();
            if (renovation == null){
                throw new NullPointerException("no renovation selected");
            }
            if (switchConfirmationBox("Are you sure you want to delete? This cannot be undone!")){
                rm.deleteRenovation(renovation);
                initialize();
            } 
        } catch (NullPointerException e){
            super.displayError("You must select a renovation first!");
        }
    } 
    
    /**
     * Switch the scene to Edit Renovation View
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToEditRenovation(ActionEvent event) throws IOException, SQLException, Exception {
        FXMLLoader loader = loadFXML("editrenovation");
        Scene editRenovationScene = new Scene(loader.load());
        
        //TODO Exception handling when no renovation is selected
        try {
            //Gets the controller of the new edit renovation View
            EditRenovationController controller = loader.getController();
            //Initializes the data of the new view with the selected renovation object
            controller.initializeRenovation(tableView.getSelectionModel().getSelectedItem());

            super.showScene(event,editRenovationScene);
        } catch (NullPointerException e){
            super.displayError("You must select a renovation first!");
        }
    }
    
    /**
     * Switch the scene to Edit Renovation View (Read-Only)
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToDetailedRenovation(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("editrenovation");
        Scene editRenovationScene = new Scene(loader.load());
        
        try {
            //Gets the controller of the new edit property View
            EditRenovationController controller = loader.getController();
            //Initializes the data of the new view with the selected renovation object
            controller.initializeRenovation(tableView.getSelectionModel().getSelectedItem());

            controller.setReadOnly();

            super.showScene(event,editRenovationScene);
        } catch (NullPointerException e){
            super.displayError("You must select a renovation first!");
        }
    }
    
    /**
     * Filters the finished renovations
     * @throws SQLException 
     */
    @FXML
    private void filterFinished() {
        tableView.setItems(filterRenovations(true));
    }
    
    /**
     * Filters the unfinished renovations
     * @throws SQLException 
     */
    @FXML
    private void filterUnfinished() {
        tableView.setItems(filterRenovations(false));
    }
    
    /**
     * Initializes the tables  
     */
    @FXML
    private void initialize() {
        contractorNameCol.setCellValueFactory(new PropertyValueFactory<Renovation,Contractor>("contractor"));
        propertyNameCol.setCellValueFactory(new PropertyValueFactory<Renovation,Property>("property"));
        renovationTypeCol.setCellValueFactory(new PropertyValueFactory<Renovation,String>("renovationType"));
        //Load test data
        tableView.setItems(getRenovations());
    }
    
    /**
     * Gets all contractors 
     * @return ObservableList of all renovations
     */
    private ObservableList<Renovation> getRenovations() {
        ObservableList<Renovation> renovations = FXCollections.observableArrayList();
        RenovationManager rm = new RenovationManager();
        renovations = rm.loadAllRenovations();
        
        return renovations;
    }
    
    /**
     * Filters finished or unfinished renovation
     * @param b True for finished, False for Unfinished
     * @return Filtered Observable List
     * @throws SQLException 
     */
    private ObservableList<Renovation> filterRenovations(Boolean b) {
        ObservableList<Renovation> renovations = getRenovations();
        ObservableList<Renovation> filtered;
        if (b)
            filtered = renovations.filtered(r -> r.getRenovationStatus());
        else
            filtered = renovations.filtered(r -> !r.getRenovationStatus());
        
        return filtered;
    }
}
