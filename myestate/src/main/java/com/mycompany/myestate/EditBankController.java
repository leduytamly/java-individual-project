/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.BankInstitution;
import com.mycompany.myestate.models.BankInstitutionManager;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 * Controller for edit bank view
 * @author leduy
 */
public class EditBankController extends ABankController {
    private BankInstitution selectedBank;
    
    /**
     * Initialize the view
     */
    @FXML
    private void initialize(){
        super.setAllListeners();
    }
    
    /**
     * Initialize the bank to pre load the data
     * @param bank Bank to pre load
     */
    public void initializeBank(BankInstitution bank){
        this.selectedBank = bank;
        bankName.setText(this.selectedBank.getInstitutionName());
        interestRate.setText(Double.toString(this.selectedBank.getInterestRate()));
    }
    
    /**
     * Sets the banks new values
     */
    private void setBank(){
        selectedBank.setInstitutionName(bankName.getText());
        selectedBank.setInterestRate(Double.parseDouble(interestRate.getText()));
    }
    
    /**
     * Makes the update in the database
     * @param event
     * @throws IOException
     */
    @FXML
    private void makeChange(ActionEvent event) throws IOException {
        if (switchConfirmationBox("Are you sure you want to make the change?")){
            try{
                /*Data validation*/
                super.validateAllFields();
                
                /*Sets the bank with new values and execute the update*/
                setBank();
                BankInstitutionManager bim = new BankInstitutionManager();
                bim.updateBank(selectedBank);
                
                /*Change the back to bank view*/
                super.switchToBanks(event);
                
            } catch (IOException e) {
                System.out.println(e);
                System.out.println("ERROR SHOWING WARNING");
            } catch(IllegalArgumentException e) {
                System.out.println(e);
                switchWarningBox("Missing values!");
            }
        }
    }
    
}
