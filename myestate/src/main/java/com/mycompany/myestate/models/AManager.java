/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

/**
 * Abstract class for manager
 * @author leduy
 */
public abstract class AManager {
    
    /**
     * Gets the connection of the database
     * @return Connection object
     */
    protected Connection getConnection() {
        Connection conn = null;
        
        /*Credential for database*/
        String url = "jdbc:mysql://127.0.0.1:3306/myestate_db";
        String username = "le";
        String password = "";
        
        try {
            conn = DriverManager.getConnection(url,username,password);
            System.out.println("Database Connected");
        } catch (SQLException e){
            System.out.println("ERROR MAKING CONNECTION");
            System.out.println(e);
        } 
        return conn;
    }
    
    /**
     * Gets the last insert primary key of the object
     * @param conn Connection object 
     * @param ps PreparedStatment object
     * @param rs ResultSet object
     * @return PK of the last row inserted in the database
     */
    protected int getLastId(Connection conn, PreparedStatement ps, ResultSet rs) {
        /*Gets the primary key of the last insert*/
        int pk = 0;
        try {
            ps = conn.prepareStatement("SELECT last_insert_id()");
            rs = ps.executeQuery();
            while(rs.next()){
                pk = rs.getInt(1);
            }
        } catch (SQLException e){
            System.out.println("ERROR GET LAST ID");
            System.out.println(e);
        }

        return pk;
    }
    
    /**
     * Closes Connection object
     * @param conn Connection to close
     */
    protected void close(Connection conn) {
        try {
            if (conn != null){
                conn.close();
            }
        } catch (SQLException e){
            System.out.println("ERROR CLOSE CONNECTION");
            System.out.println(e);
        }
    }
    
    /**
     * Close Statement object
     * @param statement Statement to close
     */
    protected void close(Statement statement){
        try{
            if (statement != null){
                statement.close();
            }
        } catch (SQLException e){
            System.out.println("ERROR CLOSE STATEMENT");
            System.out.println(e);
        }
    }
    
    /**
     * Closes ResultSet object
     * @param resultSet ResultSet to close
     */
    protected void close(ResultSet resultSet) {
        try{
            if (resultSet != null){
                resultSet.close();
            }
        } catch (SQLException e){
            System.out.println("ERROR CLOSE RESULTSET");
            System.out.println(e);
        }
    }
    
    /**
     * Close PreparedStament Object
     * @param preparedStatement PreparedStatement to close
     */
    protected void close(PreparedStatement preparedStatement){
        try {
            if (preparedStatement != null){
                preparedStatement.close();
            }
        } catch (SQLException e){
            System.out.println("ERROR CLOSE PREPAREDSTATEMENT");
            System.out.println(e);
        }
    }
    
    /**
     * Close Connection, Statement, and ResultSet
     * @param conn Connection to close
     * @param statement Statement to close
     * @param resultSet ResultSet to close
     */
    protected void close(Connection conn, Statement statement, ResultSet resultSet){
        try{
            close(conn);
            close(statement);
            close(resultSet);
        } catch (Exception e){
            System.out.println(e);
        }
    }
}
