/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;

/**
 * Class Representing a Contractor
 * @author leduy
 */
public class Contractor {
    /*All Attributes for a Contractor*/
    private int pk;
    private SimpleStringProperty contractorName;
    private SimpleStringProperty contractorCompany;
    private SimpleStringProperty contractorPhoneNumber;
    private SimpleStringProperty contractorEmail;
    private SimpleStringProperty contractorProfession;
    
    /**
     * Parameterized Constructor for Contractor used when loading a contractor
     * @param pk
     * @param contractorName Name of the Contractor
     * @param contractorCompany Name of the Company
     * @param contractorPhoneNumber Phone Number of the Contractor
     * @param contractorEmail Email of the Contractor
     * @param contractorProfession Profession(specialty) of the contractor
     */
    public Contractor(int pk, String contractorName, String contractorCompany, String contractorPhoneNumber, String contractorEmail, String contractorProfession) {
        this.pk = pk;
        this.contractorName = new SimpleStringProperty(contractorName);
        this.contractorCompany = new SimpleStringProperty(contractorCompany);
        this.contractorPhoneNumber = new SimpleStringProperty(contractorPhoneNumber);
        this.contractorEmail = new SimpleStringProperty(contractorEmail);
        this.contractorProfession = new SimpleStringProperty(contractorProfession);
    }
    
    /**
     * Parameterized Constructor for Contractor used when creating a new contractor
     * @param contractorName Name of the Contractor
     * @param contractorCompany Name of the Company
     * @param contractorPhoneNumber Phone Number of the Contractor
     * @param contractorEmail Email of the Contractor
     * @param contractorProfession Profession(specialty) of the contractor
     */
    public Contractor(String contractorName, String contractorCompany, String contractorPhoneNumber, String contractorEmail, String contractorProfession) {
        this.contractorName = new SimpleStringProperty(contractorName);
        this.contractorCompany = new SimpleStringProperty(contractorCompany);
        this.contractorPhoneNumber = new SimpleStringProperty(contractorPhoneNumber);
        this.contractorEmail = new SimpleStringProperty(contractorEmail);
        this.contractorProfession = new SimpleStringProperty(contractorProfession);
    }
    
    /*Getters for Contractor*/
    public int getPk(){
        return pk;
    }
    
    public String getContractorName() {
        return contractorName.get();
    }

    public String getContractorCompany() {
        return contractorCompany.get();
    }

    public String getContractorPhoneNumber() {
        return contractorPhoneNumber.get();
    }

    public String getContractorEmail() {
        return contractorEmail.get();
    }
    
    public String getContractorProfession(){
        return this.contractorProfession.get();
    }

    /*Setter for Contractor*/
    public void setContractorName(String contractorName) {
        this.contractorName = new SimpleStringProperty (contractorName);
    }

    public void setContractorCompany(String contractorCompany) {
        this.contractorCompany = new SimpleStringProperty (contractorCompany);
    }

    public void setContractorPhoneNumber(String contractorPhoneNumber) {
        this.contractorPhoneNumber = new SimpleStringProperty (contractorPhoneNumber);
    }

    public void setContractorEmail(String contractorEmail) {
        this.contractorEmail = new SimpleStringProperty (contractorEmail);
    }
    
    public void setContractorProfession(String contractorProfession){
        this.contractorProfession = new SimpleStringProperty(contractorProfession);
    }
    
    @Override
    public String toString(){
        return this.getContractorName() + " ; " + this.getContractorCompany();
    }
    
    /**
     * Overriding the equals method
     * @param other Other object to compare 
     * @return boolean representing if a object is equal (same to string)
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) return true ;

        if (! (other instanceof Contractor)) return false ;

        Contractor c = (Contractor) other;
        return this.toString().equals(c.toString());
    }

    /**
     * Overriding hash code method
     * @return Hash Code
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.contractorName);
        hash = 83 * hash + Objects.hashCode(this.contractorCompany);
        return hash;
    }
    
}
