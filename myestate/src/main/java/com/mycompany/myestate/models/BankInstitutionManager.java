/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Class responsible for Bank Institutions and database
 * @author leduy
 */
public class BankInstitutionManager extends AManager{
    
    /**
     * Loads all banks from the database
     * @return ObservableList of banks
     */
    public ObservableList<BankInstitution> loadAllBanks() {
        ObservableList<BankInstitution> banks = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "SELECT * FROM bank_institution";
            statement = conn.prepareStatement(query);
            resultSet = statement.executeQuery(query);
            
            /*Loops through all the Banks in the DB*/
            while (resultSet.next()){
                BankInstitution bank = new BankInstitution(resultSet.getInt("bank_id"),resultSet.getString("institution_name"), resultSet.getDouble("interest_rate"));
                banks.add(bank);
            }
            
        } catch (SQLException e){
            System.out.println("ERROR LOADING BANKS");
            System.out.println(e);
        } finally {
            /*Closes Connection,Statmenent, ResultSet */
            close(conn,statement,resultSet);
            System.out.println("Database Closed");
        }
        return banks;
    }
    
    public void insertBank(BankInstitution bank){
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "INSERT INTO bank_institution (institution_name, interest_rate)"
                    + "VALUES (?,?)";
            System.out.println(query);
            ps = conn.prepareStatement(query);
            ps.setString(1, bank.getInstitutionName());
            ps.setDouble(2, bank.getInterestRate());
            ps.executeUpdate();
            
        } catch (SQLException e){
            System.out.println("ERROR WHEN INSERTING BANK");
            System.out.println(e);
        } finally {
            /*Closes Connection,Statmenent, ResultSet */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    public void updateBank(BankInstitution bank){
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "UPDATE bank_institution SET institution_name = ?, interest_rate = ? WHERE bank_id = "+bank.getPk();
            System.out.println(query);
            /*Creating the Prepared Statment and assigning the data to update*/
            ps = conn.prepareStatement(query);
            ps.setString(1, bank.getInstitutionName());
            ps.setDouble(2, bank.getInterestRate());
            /*Execute the update */
            ps.executeUpdate();
            
        } catch (SQLException e){
            System.out.println("ERROR WHEN UPDATING BANK");
            System.out.println(e);
        } finally {
            /*Closes Connection,Prepared Statement */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    public void deleteBank(BankInstitution bank){
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "DELETE FROM bank_institution WHERE bank_id = \""+bank.getPk()+"\"";
            System.out.println(query);
            /*Creating the prepared statement and executing the delete*/
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch(SQLException e){
            System.out.println("ERROR WHEN DELETING BANK");
            System.out.println(e);
        } finally {
            /*Closing Connection and Prepared Statement*/
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
}
