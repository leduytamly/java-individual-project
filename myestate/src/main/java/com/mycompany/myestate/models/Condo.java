/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

/**
 * Condo Object
 * @author leduy
 */
public class Condo extends Property {
    private double condoFee;
    
    /**
     * Parameterized Constructor for Condo (used to load from the database)
     * @param pk Primary Key
     * @param propertyName Name of the Condo
     * @param propertyAddress Address of the Condo 
     * @param propertyCost Cost of the Condo
     * @param propertyRentalPrice Rental price of the Condo
     * @param propertyTaxRate Tax Rate of the Condo
     * @param propertySchoolTaxRate School Tax Rate of the Condo
     * @param insurance Insurance of the Condo
     * @param condoFee Condo fee
     */
    public Condo(int pk, String propertyName,String propertyAddress, double propertyCost, double propertyRentalPrice, double propertyTaxRate, double propertySchoolTaxRate, Insurance insurance, double condoFee){
        super(pk,propertyName,"Condo",propertyAddress,propertyCost,1,propertyRentalPrice,propertyTaxRate,propertySchoolTaxRate,insurance);
        this.condoFee = condoFee;
    }
    
    /**
     * Parameterized Constructor for Condo (Used when creating a new condo) 
     * @param propertyName Name of the Condo
     * @param propertyAddress Address of the Condo 
     * @param propertyCost Cost of the Condo
     * @param propertyRentalPrice Rental price of the Condo
     * @param propertyTaxRate Tax Rate of the Condo
     * @param propertySchoolTaxRate School Tax Rate of the Condo
     * @param insurance Insurance of the Condo
     * @param condoFee Condo fee
     */
    public Condo(String propertyName, String propertyAddress, double propertyCost, double propertyRentalPrice, double propertyTaxRate, double propertySchoolTaxRate, Insurance insurance, double condoFee){
        super(propertyName,"Condo",propertyAddress,propertyCost,1,propertyRentalPrice,propertyTaxRate,propertySchoolTaxRate,insurance);
        this.condoFee = condoFee;
    } 
    
    /*Getter for Condo*/
    public double getCondoFee() {
        return condoFee;
    }

    /*Setter for Condo*/
    public void setCondoFee(double condoFee) {
        this.condoFee = condoFee;
    }
}


