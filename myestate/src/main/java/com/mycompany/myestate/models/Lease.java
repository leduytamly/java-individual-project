/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

/**
 * Object for a lease
 * @author leduy
 */
public class Lease {
    private int pk;
    private LocalDate startdate;
    private LocalDate enddate;
    private String sourcePath;
    private String destinationPath;

    /**
     * Parameterized Constructor with pk used for loading from database
     * @param pk Primary Key
     * @param startdate Start date of the lease
     * @param enddate End date of the lease
     * @param destinationPath Destination path of the lease file
     */
    public Lease(int pk, LocalDate startdate, LocalDate enddate, String destinationPath) {
        this.pk = pk;
        this.startdate = startdate;
        this.enddate = enddate;
        this.destinationPath = destinationPath;
    }

    /**
     * Parameterized Constructor without pk used for creating a new lease object
     * @param startdate Start date of the lease
     * @param enddate  end date of the lease 
     * @param sourcePath Source path of the lease file
     */
    public Lease(LocalDate startdate, LocalDate enddate, String sourcePath) {
        this.startdate = startdate;
        this.enddate = enddate;
        this.sourcePath = sourcePath;
    }
    
    /*Getters for lease object*/
    public int getPk(){
        return pk;
    }
    
    public LocalDate getStartdate() {
        return startdate;
    }

    public LocalDate getEnddate() {
        return enddate;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    /*Setters for lease object*/
    public void setStartdate(LocalDate startdate) {
        this.startdate = startdate;
    }

    public void setEnddate(LocalDate enddate) {
        this.enddate = enddate;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public void setDestinationPath(String destinationPath) {
        this.destinationPath = destinationPath;
    }
    
    /**
     * Saves the source file to a destination path
     * @param filename Name of the new file
     */
    public void saveFile(String filename){
        try {
            File source = new File(sourcePath);
            /*Source path hardcoded for now*/
            File destination = new File("C:\\Users\\leduy\\courses\\java410\\DestinationLease\\"+filename+".pdf");
            
            /*If the file already exists it will make a archive copy of the current lease*/
            if(destination.exists()){
                /*Gets today's date*/
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");  
                Date date = new Date();  
                /*Archives the file*/
                File archive = new File("C:\\Users\\leduy\\courses\\java410\\Archive\\"+filename+"_archive_"+formatter.format(date)+".pdf");
                Files.copy(destination.toPath(),archive.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }

            destinationPath = destination.getAbsolutePath();

            Files.copy(source.toPath(),destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
            System.out.println("File backed up");
        } catch (IOException e){
            System.out.println(e);
            System.out.println("Error when copy");
        }

    }
    
     /**
     * Overriding the equals method
     * @param other Other object to compare 
     * @return boolean representing if a object is equal (same to string)
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) return true ;

        if (! (other instanceof Lease)) return false ;

        Lease l = (Lease) other;
        return this.destinationPath.equals(l.destinationPath);
    }

    /**
     * Overriding HashCode method
     * @return Hash Code
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.destinationPath);
        return hash;
    }
}
