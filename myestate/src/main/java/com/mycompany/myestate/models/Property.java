/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;

/**
 *Class Representing a Property
 * @author leduy
 */
public abstract class Property {
    
    //All Attributes for a property
    private int pk;
    private SimpleStringProperty propertyName;
    private SimpleStringProperty propertyType;
    private SimpleStringProperty propertyAddress;
    private double propertyCost;
    private int propertyDoors;
    private double propertyRentalPrice;
    private double propertyTaxRate;
    private double propertySchoolTaxRate;
    private Insurance insurance;
    
    /**
     * Parameterized Constructor with primary key (Used when loading the object
     * from the database)
     * @param pk
     * @param propertyName Name of the Property
     * @param propertyType Type of the Property (Condo, Appartment, Duplex, etc. )
     * @param propertyAddress Address of the Property
     * @param propertyCost Cost of the Property
     * @param propertyDoors Number of Doors the Property has
     * @param propertyRentalPrice The rental price for the Property
     * @param propertyTaxRate Tax Rate for the Property
     * @param propertySchoolTaxRate School Tax Rate for the Property
     * @param insurance Insurance Object
     */
    public Property(int pk, String propertyName, String propertyType, String propertyAddress, double propertyCost, int propertyDoors, double propertyRentalPrice, double propertyTaxRate, double propertySchoolTaxRate, Insurance insurance) {
        this.pk = pk;
        this.propertyName = new SimpleStringProperty(propertyName);
        this.propertyType = new SimpleStringProperty(propertyType);
        this.propertyAddress = new SimpleStringProperty(propertyAddress);
        this.propertyCost = propertyCost;
        this.propertyDoors = propertyDoors;
        this.propertyRentalPrice = propertyRentalPrice;
        this.propertyTaxRate = propertyTaxRate;
        this.propertySchoolTaxRate = propertySchoolTaxRate;
        this.insurance = insurance;
    }
    
    /**
     * Parameterized Constructor without primary key (Used when creating a new object)
     * It does not have a pk yet
     * @param propertyName Name of the Property
     * @param propertyType Type of the Property (Condo, Appartment, Duplex, etc. )
     * @param propertyAddress Address of the Property
     * @param propertyCost Cost of the Property
     * @param propertyDoors Number of Doors the Property has
     * @param propertyRentalPrice The rental price for the Property
     * @param propertyTaxRate Tax Rate for the Property
     * @param propertySchoolTaxRate School Tax Rate for the Property
     * @param insurance Insurance Object
     */
    public Property(String propertyName, String propertyType, String propertyAddress, double propertyCost, int propertyDoors, double propertyRentalPrice, double propertyTaxRate, double propertySchoolTaxRate, Insurance insurance) {
        this.propertyName = new SimpleStringProperty(propertyName);
        this.propertyType = new SimpleStringProperty(propertyType);
        this.propertyAddress = new SimpleStringProperty(propertyAddress);
        this.propertyCost = propertyCost;
        this.propertyDoors = propertyDoors;
        this.propertyRentalPrice = propertyRentalPrice;
        this.propertyTaxRate = propertyTaxRate;
        this.propertySchoolTaxRate = propertySchoolTaxRate;
        this.insurance = insurance;
    }
    
    /*Getters For Property*/
    public int getPk(){
        return pk;
    }
    
    public String getPropertyName() {
        return propertyName.get();
    }

    public String getPropertyType() {
        return propertyType.get();
    }

    public String getPropertyAddress() {
        return propertyAddress.get();
    }

    public double getPropertyCost() {
        return propertyCost;
    }

    public int getPropertyDoors() {
        return propertyDoors;
    }

    public double getPropertyRentalPrice() {
        return propertyRentalPrice;
    }

    public double getPropertyTaxRate() {
        return propertyTaxRate;
    }

    public double getPropertySchoolTaxRate() {
        return propertySchoolTaxRate;
    }
    
    public Insurance getInsurance() {
        return insurance;
    }

    /*Setters for property object*/
    public void setPropertyName(String propertyName) {
        this.propertyName = new SimpleStringProperty(propertyName);
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = new SimpleStringProperty(propertyType);
    }

    public void setPropertyAddress(String propertyAddress) {
        this.propertyAddress = new SimpleStringProperty(propertyAddress);
    }

    public void setPropertyCost(double propertyCost) {
        this.propertyCost = propertyCost;
    }

    public void setPropertyDoors(int propertyDoors) {
        this.propertyDoors = propertyDoors;
    }

    public void setPropertyRentalPrice(double propertyRentalPrice) {
        this.propertyRentalPrice = propertyRentalPrice;
    }

    public void setPropertyTaxRate(double propertyTaxRate) {
        this.propertyTaxRate = propertyTaxRate;
    }

    public void setPropertySchoolTaxRate(double propertySchoolTaxRate) {
        this.propertySchoolTaxRate = propertySchoolTaxRate;
    }

    public void setInsurance(Insurance insurance){
        this.insurance = insurance;
    }
    /**
     * Overriding the toString method
     * @return String
     */
    @Override
    public String toString(){
        return getPropertyName() +" ; "+ getPropertyAddress();
    }
    
    /**
     * Overriding the equals method
     * @param other Other object to compare 
     * @return boolean representing if a object is equal (same to string)
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) return true ;

        if (! (other instanceof Property)) return false ;

        Property p = (Property) other;
        return this.toString().equals(p.toString());
    }

    /**
     * Overriding the hash code method
     * @return Hash Code
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.propertyName);
        hash = 41 * hash + Objects.hashCode(this.propertyAddress);
        return hash;
    }
}
