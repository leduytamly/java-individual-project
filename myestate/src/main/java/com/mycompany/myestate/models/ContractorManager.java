/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Class responsible for contractor and database
 * @author leduy
 */
public class ContractorManager extends AManager{
    
    /**
     * Loads all contractors from the database
     * @return ObservableList of all contractors
     */
    public ObservableList<Contractor> loadAllContractors() {
        ObservableList<Contractor> contractors = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "SELECT * FROM contractor";
            statement = conn.prepareStatement(query);
            resultSet = statement.executeQuery(query);
            
            /*Loops through all properties in the DB*/
            while (resultSet.next()){

                /*Creating the contractor object*/
                Contractor contractor = new Contractor(resultSet.getInt("contractor_id"),
                        resultSet.getString("contractor_name"),
                        resultSet.getString("company"),
                        resultSet.getString("phone_number"),
                        resultSet.getString("email"),
                        resultSet.getString("profession"));
                        
                /*Adding the property to the ObservableList*/
                contractors.add(contractor);
            }
        } catch (SQLException e){
            System.out.println("ERROR LOADING CONTRACTORS");
            System.out.println(e);
        } finally {
            /*Closes Connection,Statmenent, ResultSet */
            close(conn,statement,resultSet);
            System.out.println("Database Closed");
        }
        /*Returning the ObservableList*/
        return contractors;
    }
    
    /**
     * Updates a contractor update in the database with new values
     * @param contractor Contractor object to update
     */
    public void updateContractor(Contractor contractor) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "UPDATE contractor SET contractor_name = ?, company = ?, phone_number = ?,"
                    + " email = ?, profession = ? WHERE contractor_id = "+contractor.getPk();
            System.out.println(query);
            /*Creating the Prepared Statment and assigning the data to update*/
            ps = conn.prepareStatement(query);
            ps.setString(1,contractor.getContractorName());
            ps.setString(2,contractor.getContractorCompany());
            ps.setString(3, contractor.getContractorPhoneNumber());
            ps.setString(4, contractor.getContractorEmail());
            ps.setString(5, contractor.getContractorProfession());
            /*Execute the update */
            ps.executeUpdate();
            
        } catch (SQLException e){
            System.out.println("ERROR WHEN UPDATING CONTRACTOR");
            System.out.println(e);
        } finally {
            /*Closes Connection,Prepared Statement */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Deletes a contractor from the database
     * @param contractor Contractor to delete
     */
    public void deleteContractor(Contractor contractor) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "DELETE FROM contractor WHERE contractor_id = \""+contractor.getPk()+"\"";
            System.out.println(query);
            /*Creating the prepared statement and executing the delete*/
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch(SQLException e){
            System.out.println("ERROR WHEN DELETING CONTRACTOR");
            System.out.println(e);
        } finally {
            /*Closing Connection and Prepared Statement*/
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Inserts a contractor in the database
     * @param contractor
     */
    public void insertContractor(Contractor contractor) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "INSERT INTO contractor (contractor_name, company, phone_number, email, profession)"
                    + "VALUES (?,?,?,?,?)";
            System.out.println(query);
            ps = conn.prepareStatement(query);
            ps.setString(1, contractor.getContractorName());
            ps.setString(2, contractor.getContractorCompany());
            ps.setString(3,contractor.getContractorPhoneNumber());
            ps.setString(4,contractor.getContractorEmail());
            ps.setString(5,contractor.getContractorProfession());
            ps.executeUpdate();
            
        } catch (SQLException e){
            System.out.println("ERROR WHEN INSERTING CONTRACTOR");
            System.out.println(e);
        } finally {
            /*Closes Connection,Statmenent, ResultSet */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
}
