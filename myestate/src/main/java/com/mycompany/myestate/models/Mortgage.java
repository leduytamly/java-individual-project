/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;
import java.time.LocalDate;
import javafx.beans.property.SimpleStringProperty;

/**
 * Class Representing a Mortgage
 * @author leduy
 */
public class Mortgage {
    
    /*All Attributes for a Mortgage*/
    private int pk;
    private SimpleStringProperty propertyName;
    private LocalDate nextPaymentDate;
    private LocalDate startDate;
    private LocalDate endDate;
    private double downPayment;
    private BankInstitution bank;
    
    /**
     * Parameterized Constructor for mortgage (used when loading a mortgage)
     * @param pk
     * @param propertyName Name of the Property
     * @param nextPaymentDate Date of the nextPayment
     * @param startDate Start date of the Mortgage
     * @param endDate End date of the Mortgage
     * @param downPayment  Down Payment for the Property
     * @param bank Bank Object
     */
    public Mortgage(int pk, String propertyName, LocalDate nextPaymentDate, LocalDate startDate, LocalDate endDate, double downPayment, BankInstitution bank) {
        this.pk = pk;
        this.propertyName = new SimpleStringProperty(propertyName);
        this.nextPaymentDate = nextPaymentDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.downPayment = downPayment;
        this.bank = bank;
    }
    
    /**
     * Parameterized Constructor for mortgage (used when creating a mortgage)
     * @param propertyName
     * @param nextPaymentDate
     * @param startDate
     * @param endDate
     * @param downPayment
     * @param bank 
     */
    public Mortgage(String propertyName, LocalDate nextPaymentDate, LocalDate startDate, LocalDate endDate, double downPayment, BankInstitution bank) {
        this.propertyName = new SimpleStringProperty(propertyName);
        this.nextPaymentDate = nextPaymentDate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.downPayment = downPayment;
        this.bank = bank;
    }
    
    /*Getters for Mortgages*/
    public int getPk(){
        return pk;
    }
    
    public String getPropertyName() {
        return propertyName.get();
    }

    public LocalDate getNextPaymentDate() {
        return nextPaymentDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public double getDownPayment() {
        return downPayment;
    }
    
    public BankInstitution getBank(){
        return this.bank;
    }

    /*Setters for mortgage*/
    public void setPropertyName(String propertyName) {
        this.propertyName = new SimpleStringProperty(propertyName);
    }

    public void setNextPaymentDate(LocalDate nextPaymentDate) {
        this.nextPaymentDate = nextPaymentDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public void setDownPayment(double downPayment) {
        this.downPayment = downPayment;
    }

    public void setBank(BankInstitution bank) {
        this.bank = bank;
    }
    
    
}
