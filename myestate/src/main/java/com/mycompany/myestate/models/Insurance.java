/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;

/**
 *Class that represents an Insurance
 * @author leduy
 */
public class Insurance {
    /*All attributes for an Insurance company*/
    int pk;
    SimpleStringProperty insuranceName;
    double insuranceAmount;
    
    /**
     * Parameterized constructor for Insurance (used when loading insurance from
     * database)
     * @param pk Primary Key of the property object
     * @param InsuranceName Name of the Insurance Company
     * @param InsuranceAnmount Insurance Amount
     */
    public Insurance(int pk, String InsuranceName, double InsuranceAnmount) {
        this.pk = pk;
        this.insuranceName = new SimpleStringProperty(InsuranceName);
        this.insuranceAmount = InsuranceAnmount;
    }
    
    /**
     * Parameterized constructor for Insurance (used when inserting into the database)
     * @param InsuranceName
     * @param InsuranceAnmount 
     */
    public Insurance(String InsuranceName, double InsuranceAnmount) {
        this.insuranceName = new SimpleStringProperty(InsuranceName);
        this.insuranceAmount = InsuranceAnmount;
    }
    
    /*Getters for Insurance*/
    public int getPk(){
        return pk;
    }
    
    public String getInsuranceName() {
        return insuranceName.get();
    }
    
    public double getInsuranceAmount() {
        return insuranceAmount;
    }

    /*Setters for Insurance*/
    public void setInsuranceName(String insuranceName) {
        this.insuranceName = new SimpleStringProperty(insuranceName);
    }

    public void setInsuranceAmount(double insuranceAmount) {
        this.insuranceAmount = insuranceAmount;
    }
    
    /**
     * Overriding equals method
     * @param other Other object to compare
     * @return boolean representing if 2 objects are equal (same pk)
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) return true ;

        if (! (other instanceof Insurance)) return false ;

        Insurance i = (Insurance) other;
        return this.pk == i.pk;
    }
    
    /**
     * Overriding the hashCode method
     * @return Hash Code
     */
    @Override
    public int hashCode() {
        return Objects.hash(insuranceName, insuranceAmount);
    }

    
}
