/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;

/**
 * Class Representing a Bank Institution
 * @author leduy
 */
public class BankInstitution {
    /*Attributes for a Bank Institution*/
    private int pk;
    private SimpleStringProperty institutionName;
    private double interestRate;

    /**
     * Parameterized Constructor for Bank Institution used when loading banks from 
     * database
     * @param pk Primary key of the bank
     * @param institutionName Name of the bank institution
     * @param interestRate  Interest rate of the bank institution
     */
    public BankInstitution(int pk, String institutionName, double interestRate) {
        this.pk = pk;
        this.institutionName = new SimpleStringProperty(institutionName);
        this.interestRate = interestRate;
    }
    
    /**
     * Parameterized Constructor for Bank Institution (used when creating a new bank)
     * @param institutionName
     * @param interestRate 
     */
    public BankInstitution(String institutionName, double interestRate) {
        this.institutionName = new SimpleStringProperty(institutionName);
        this.interestRate = interestRate;
    }

    /*Getters for BankInstitution*/
    public int getPk(){
        return pk;
    }
    
    public String getInstitutionName() {
        return institutionName.get();
    }

    public double getInterestRate() {
        return interestRate;
    }
    
    /*Setters for institution*/
    public void setInstitutionName(String institutionName){
        this.institutionName = new SimpleStringProperty(institutionName);
    }
    
    public void setInterestRate(Double rate){
        this.interestRate = rate;
    }
    
    /**
     * Overriding the toString method
     * @return bank name and interest rate
     */
    @Override
    public String toString(){
        return getInstitutionName()+": "+Double.toString(getInterestRate())+"%";
    }
    
    /**
     * Overriding the equals method
     * @param other Other object to compare 
     * @return boolean representing if a object is equal (same to string)
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) return true ;

        if (! (other instanceof BankInstitution)) return false ;

        BankInstitution bi = (BankInstitution) other;
        return this.toString().equals(bi.toString());
    }
    
    /**
     * Overriding the hash code method
     * @return Hash Code
     */
    @Override
    public int hashCode() {
        return Objects.hash(institutionName, interestRate);
    }
}
