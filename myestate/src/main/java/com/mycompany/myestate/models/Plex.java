/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

/**
 * Plex Object
 * @author leduy
 */
public class Plex extends Property {

    /**
     *  Parameterized Constructor for plex (Used when loading from the database)
     * @param pk Primary Key of the Plex
     * @param propertyName Name of the Plex
     * @param propertyAddress Address of the Plex
     * @param propertyCost Cost of the Plex
     * @param propertyDoors Number of units in the Plex
     * @param propertyRentalPrice  Rental price of the Plex
     * @param propertyTaxRate Tax Rate of the Plex
     * @param propertySchoolTaxRate School Tax Rate
     * @param insurance Insurance of the Plex
     */
    public Plex(int pk, String propertyName, String propertyAddress, double propertyCost, int propertyDoors, double propertyRentalPrice, double propertyTaxRate, double propertySchoolTaxRate, Insurance insurance) {
        super(pk, propertyName, "Plex", propertyAddress, propertyCost, propertyDoors, propertyRentalPrice, propertyTaxRate, propertySchoolTaxRate, insurance);
    }
    
    /**
     * Parameterized Constructor for plex (Used when creating a plex )
     * @param propertyName Name of the Plex
     * @param propertyAddress Address of the Plex
     * @param propertyCost Cost of the Plex
     * @param propertyDoors Number of units in the Plex
     * @param propertyRentalPrice  Rental price of the Plex
     * @param propertyTaxRate Tax Rate of the Plex
     * @param propertySchoolTaxRate School Tax Rate
     * @param insurance Insurance of the Plex 
     */
    public Plex(String propertyName, String propertyAddress, double propertyCost, int propertyDoors, double propertyRentalPrice, double propertyTaxRate, double propertySchoolTaxRate, Insurance insurance) {
        super(propertyName, "Plex", propertyAddress, propertyCost, propertyDoors, propertyRentalPrice, propertyTaxRate, propertySchoolTaxRate, insurance);
    }
    
}
