/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Class responsible for tenants and database
 * @author leduy
 */
public class TenantManager extends AManager {

    private PropertyManager pm = new PropertyManager();
    
    /**
     * Loads all Tenants from the database
     * @return ObservableList of all contractors
     */
    public ObservableList<Tenant> loadAllTenant() {
        ObservableList<Tenant> tenants = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "SELECT * FROM tenant JOIN lease USING(tenant_id) JOIN property USING(property_id)";
            statement = conn.prepareStatement(query);
            rs = statement.executeQuery(query);
            
            /*Loops through all properties in the DB*/
            while (rs.next()){
                /*Creating lease object*/
                Lease lease = new Lease(rs.getInt("lease_id"), 
                        LocalDate.parse(rs.getString("start_date")), 
                        LocalDate.parse(rs.getString("end_date")),
                        rs.getString("lease_path"));
                
                /*Creating the property object*/
                Property property = pm.createPropertyByType(rs.getString("type"),rs,null);

                /*Creating the tenant object*/
                Tenant tenant = new Tenant(rs.getInt("tenant_id"),
                        rs.getString("fullname"), property ,
                        rs.getString("phone_number"),
                        rs.getString("email"),
                        LocalDate.parse(rs.getString("rent_start_date")),
                        LocalDate.parse(rs.getString("rent_end_date")),
                        rs.getDouble("yearly_income"),
                        rs.getDouble("income_debt_ratio"),
                        rs.getBoolean("has_paid_rent"), rs.getString("payment_type"),lease);
     
                /*Adding the property to the ObservableList*/
                tenants.add(tenant);
            }
        } catch (SQLException e){
            System.out.println("ERROR LOADING Tenants");
            System.out.println(e);
        } finally {
            /*Closes Connection,Statmenent, ResultSet */
            close(conn,statement,rs);
            System.out.println("Database Closed");
        }
        /*Returning the ObservableList*/
        return tenants;
    }
    
    /**
     * Inserts a tenant into the database
     * @param tenant Tenant to insert
     * @return the pk of the newly added tenant
     */
    public int insertTenant(Tenant tenant) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int pk = 0;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "INSERT INTO tenant (property_id,fullname,phone_number,"
                    + "email,rent_start_date,rent_end_date,yearly_income,"
                    + "income_debt_ratio,has_paid_rent,payment_type)" +
                    "VALUES(?,?,?,?,?,?,?,?,?,?);";
            System.out.println(query);
            ps = conn.prepareStatement(query);
            /*Fields to insert*/
            ps.setInt(1,tenant.getProperty().getPk());
            ps.setString(2, tenant.getTenantFullName());
            ps.setString(3, tenant.getTenantPhoneNumber());
            ps.setString(4, tenant.getTenantEmail());
            ps.setString(5, tenant.getTenantRentStart().format(DateTimeFormatter.ISO_DATE));
            ps.setString(6, tenant.getTenantRentEnd().format(DateTimeFormatter.ISO_DATE));
            ps.setDouble(7, tenant.getTenantYearlyIncome());
            ps.setDouble(8, tenant.getTenantIncomeDebtRatio());
            ps.setBoolean(9,tenant.getTenantStatus());
            ps.setString(10, tenant.getTenantPaymentMethond());
            /*Execute the insert*/
            ps.executeUpdate();
            
            pk = super.getLastId(conn, ps, rs);
        } catch (SQLException e){
            System.out.println("ERROR WHEN INSERTING TENANT");
            System.out.println(e);
        } finally {
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
        return pk;
    }
    
    /**
     * Inserts a lease into the database
     * @param lease Lease to be added in the database
     * @param tenantId Tenant id that the lease is tied too
     */
    public void insertLease(Lease lease, int tenantId) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "INSERT INTO lease (tenant_id,start_date,end_date,lease_path) " +
                    "VALUES (?,?,?,?)";
            System.out.println(query);
            ps = conn.prepareStatement(query);
            /*Fields to insert*/
            ps.setInt(1, tenantId);
            ps.setString(2, lease.getStartdate().format(DateTimeFormatter.ISO_DATE));
            ps.setString(3, lease.getEnddate().format(DateTimeFormatter.ISO_DATE));
            ps.setString(4, lease.getDestinationPath());
            /*Execute insert*/
            ps.executeUpdate();
        } catch (SQLException e){
            System.out.println("ERROR WHEN INSERTING LEASE");
            System.out.println(e);
        } finally {
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Updates a tenant object in the database
     * @param tenant Tenant object to update
     */
    public void updateTenant(Tenant tenant) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "UPDATE tenant SET property_id = ?, fullname = ?, phone_number = ?, "
                    + "email = ?, rent_start_date = ?, rent_end_date = ?, yearly_income = ?, "
                    + "income_debt_ratio = ?, has_paid_rent = ?, payment_type = ?"
                    + "WHERE tenant_id = "+tenant.getPk();
            System.out.println(query);
            ps = conn.prepareStatement(query);
            /*Fields to udpate*/
            ps.setInt(1,tenant.getProperty().getPk());
            ps.setString(2, tenant.getTenantFullName());
            ps.setString(3, tenant.getTenantPhoneNumber());
            ps.setString(4, tenant.getTenantEmail());
            ps.setString(5, tenant.getTenantRentStart().format(DateTimeFormatter.ISO_DATE));
            ps.setString(6, tenant.getTenantRentEnd().format(DateTimeFormatter.ISO_DATE));
            ps.setDouble(7, tenant.getTenantYearlyIncome());
            ps.setDouble(8, tenant.getTenantIncomeDebtRatio());
            ps.setBoolean(9,tenant.getTenantStatus());
            ps.setString(10, tenant.getTenantPaymentMethond());
            /*Execute the update*/
            ps.executeUpdate();
        } catch (SQLException e){
            System.out.println("ERROR WHEN UPDATING TENANT");
            System.out.println(e);
        } finally {
            /*Closes Connection,Prepared Statement */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Updates a lease object in the database
     * @param lease Lease object to update
     */
    public void updateLease(Lease lease) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "UPDATE lease SET start_date = ?, end_date = ?, lease_path = ? "
                    + " WHERE lease_id = "+lease.getPk();
            System.out.println(query);
            ps = conn.prepareStatement(query);
            /*Fields to update*/
            ps.setString(1,lease.getStartdate().format(DateTimeFormatter.ISO_DATE));
            ps.setString(2, lease.getEnddate().format(DateTimeFormatter.ISO_DATE));
            ps.setString(3, lease.getDestinationPath());
            /*Execute the update */
            ps.executeUpdate();
        } catch (SQLException e){
            System.out.println("ERROR WHEN UPDATING LEASE");
            System.out.println(e);
        } finally {
            /*Closes Connection,Prepared Statement */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Deletes a tenant from the database
     * @param tenant Property Object to delete
     */
    public void deleteTenant(Tenant tenant) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "DELETE FROM tenant WHERE tenant_id = \""+tenant.getPk()+"\"";
            System.out.println(query);
            /*Creating the prepared statement and executing the delete*/
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch(SQLException e){
            System.out.println("ERROR WHEN DELETING Tenant");
            System.out.println(e);
        } finally {
            /*Closing Connection and Prepared Statement*/
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Gets the number of tenants in a given property
     * @param property Property to check number occupied 
     * @return Number of occupied units in a property
     */
    public int getNumberOfTenantForAProperty(Property property) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int count = 0;
        
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "SELECT COUNT(*) FROM tenant JOIN property USING(property_id) WHERE address = \""+property.getPropertyAddress()+"\"";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery(query);
            
            /*Loops through all properties in the DB*/
            while (rs.next()){
                count = rs.getInt("COUNT(*)");
            }
        } catch (SQLException e){
            System.out.println("ERROR GETTING COUNT TENANT");
            System.out.println(e);
        } finally {
            close(conn,ps,rs);
            System.out.println("Database Closed");
        }
        return count;
    }
    
    /**
     * Gets the number of tenants in a given property
     * @param property Property to check number occupied 
     * @return Number of occupied units in a property
     */
    public HashSet<String> getTenantNamesForAProperty(Property property) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashSet<String> tenantList = new HashSet<String>();
        
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "SELECT fullname FROM property JOIN tenant USING(property_id) WHERE property_id = "+property.getPk();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery(query);
            
            /*Loops through all properties in the DB*/
            while (rs.next()){
                tenantList.add(rs.getString("fullname"));
            }
        } catch (SQLException e){
            System.out.println("ERROR GETTING COUNT TENANT");
            System.out.println(e);
        } finally {
            close(conn,ps,rs);
            System.out.println("Database Closed");
        }
        return tenantList;
    }
}
