/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Property Manager Class responsible for properties and the database
 * @author leduy
 */
public class PropertyManager extends AManager {
    
    /**
     * Gets all properties that is in the database
     * @return ObservableList<Property> with all properties
     */
    public ObservableList<Property> loadAllProperties() {
        ObservableList<Property> properties = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "SELECT * FROM property JOIN insurance USING(insurance_id)";
            statement = conn.prepareStatement(query);
            resultSet = statement.executeQuery(query);
            
            /*Loops through all properties in the DB*/
            while (resultSet.next()){
                /*Gets the type of the property*/
                String propertyType = resultSet.getString("type");
                
                /*Creates an insurance object*/
                Insurance insurance = new Insurance(resultSet.getString("insurance_name"),resultSet.getDouble("insurance_amount"));
           
                /*Creating the property object*/
                Property property = createPropertyByType(propertyType,resultSet,insurance);
                
                /*Adding the property to the ObservableList*/
                properties.add(property);
            }
        } catch (SQLException e){
            System.out.println("ERROR LOADING PROPERTIES");
            System.out.println(e);
        } finally {
            /*Closes Connection,Statmenent, ResultSet */
            close(conn,statement,resultSet);
            System.out.println("Database Closed");
        }
        /*Returning the ObservableList*/
        return properties;
    }
    
    /**
     * Updates a property object into the database
     * @param property Property Object to update
     */
    public void updateProperty(Property property) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = getUpdateQueryByPropertyType(property);
            System.out.println(query);
            /*Creating the Prepared Statment and assigning the data to update common to all*/
            ps = conn.prepareStatement(query);
            ps.setString(1,property.getPropertyName());
            ps.setString(2,property.getPropertyType());
            ps.setString(3, property.getPropertyAddress());
            ps.setDouble(4, property.getPropertyCost());
            ps.setInt(5, property.getPropertyDoors());
            ps.setDouble(6, property.getPropertyRentalPrice());
            ps.setDouble(7, property.getPropertyTaxRate());
            ps.setDouble(8, property.getPropertySchoolTaxRate());
            
            setPreparedStatementValueByPropertyType(ps, property,9);
            /*Execute the update */
            ps.executeUpdate();
            
        } catch (SQLException e){
            System.out.println("ERROR WHEN UPDATING PROPERTY");
            System.out.println(e);
        } finally {
            /*Closes Connection,Prepared Statement */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Deletes a property from the database
     * @param property Property Object to delete
     */
    public void deleteProperty(Property property) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "DELETE FROM property WHERE property_id = \""+property.getPk()+"\"";
            System.out.println(query);
            /*Creating the prepared statement and executing the delete*/
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch(SQLException e){
            System.out.println("ERROR WHEN DELETING Property");
            System.out.println(e);
        } finally {
            /*Closing Connection and Prepared Statement*/
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Inserts a property object into the database
     * @param property Property Object to insert
     * @param insuranceId Insurance PK
     * @param mortgageId Mortgage PK
     */
    public void insertProperty(Property property, int insuranceId, int mortgageId) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = getInsertQueryByPropertyType(property);
            System.out.println(query);
            
            ps = conn.prepareStatement(query);
            /*FK for insurance and mortgage*/
            ps.setInt(1, insuranceId);
            ps.setInt(2, mortgageId);
            /*Property common attributes to all properties*/
            ps.setString(3,property.getPropertyName());
            ps.setString(4,property.getPropertyType());
            ps.setString(5, property.getPropertyAddress());
            ps.setDouble(6, property.getPropertyCost());
            ps.setInt(7, property.getPropertyDoors());
            ps.setDouble(8, property.getPropertyRentalPrice());
            ps.setDouble(9, property.getPropertyTaxRate());
            ps.setDouble(10, property.getPropertySchoolTaxRate());
            
            /*Sets additional values for specfic property type*/
            setPreparedStatementValueByPropertyType(ps,property,11);
            ps.executeUpdate();
            
        } catch (SQLException e){
            System.out.println("ERROR WHEN INSERTING PROPERTY");
            System.out.println(e);
        } finally {
            /*Closes Connection,Statmenent, ResultSet */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Updates an insurance object into the database
     * @param insurance Insurance Object to update
     */
    public void updateInsurance(Insurance insurance) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "UPDATE insurance SET insurance_name = ?, insurance_amount = ?"
                    + " WHERE insurance_id = "+ insurance.getPk();
            System.out.println(query);
            /*Creating the prepared statement and assigning the data to update*/
            ps = conn.prepareStatement(query);
            ps.setString(1, insurance.getInsuranceName());
            ps.setDouble(2, insurance.getInsuranceAmount());
            /*Execute the update*/
            ps.executeUpdate();
        } catch(SQLException e){
            System.out.println("ERROR WHEN UPDATING INSURANCE");
            System.out.println(e);
        } finally {
            /*Closes Connection and Prepared Statement*/
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Inserts a new insurance into the database
     * @param insurance Insurance Object to insert
     * @return pk Primary Key of the newly inserted insurance
     */
    public int insertInsurance(Insurance insurance) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs =  null;
        int pk = 0;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "INSERT INTO insurance (insurance_name,insurance_amount) VALUES(?,?)";
            System.out.println(query);
            /*Creating the prepared statement and assigning the data to insert*/
            ps = conn.prepareStatement(query);
            ps.setString(1, insurance.getInsuranceName());
            ps.setDouble(2, insurance.getInsuranceAmount());
            /*Executing the insert*/
            ps.executeUpdate();
            
            /*Gets the last inserted id*/
            pk = super.getLastId(conn, ps, rs);
            
        } catch(SQLException e){
            System.out.println("ERROR WHEN UPDATING INSURANCE");
            System.out.println(e);
        } finally {
            /*Closing Connectino, PreparedStatement, ResultSet*/
            close(conn,ps,rs);
            System.out.println("Database Closed");
        }
        return pk;
    }
    
    /**
     * Create a property object depending on the type 
     * @param type Type of property to create
     * @param rs ResultSet of the query
     * @param insurance Insurance object
     * @return Property object which can be a House, Condo, or Plex
     * @throws SQLException 
     */
    public Property createPropertyByType(String type, ResultSet rs, Insurance insurance) throws SQLException{
        switch (type) {
            case "Condo":
                return new Condo(rs.getInt("property_id"),rs.getString("property_name"),rs.getString("address"),
                        rs.getDouble("cost"),rs.getDouble("rental_price"),
                        rs.getDouble("property_tax_rate"),rs.getDouble("school_tax_rate"),
                        insurance, rs.getDouble("condo_fee"));
            case "House":
                return new House(rs.getInt("property_id"),rs.getString("property_name"),rs.getString("address"),
                        rs.getDouble("cost"),rs.getDouble("rental_price"),
                        rs.getDouble("property_tax_rate"),rs.getDouble("school_tax_rate"),
                        insurance);
            default:
                return new Plex(rs.getInt("property_id"),rs.getString("property_name"),rs.getString("address"),
                        rs.getDouble("cost"),rs.getInt("nber_spots"),rs.getDouble("rental_price"),
                        rs.getDouble("property_tax_rate"),rs.getDouble("school_tax_rate"),
                        insurance);
        }
    }
    
    /**
     * Gets the query of the update depending on the type of property given
     * @param property Property to get the query 
     * @return Update query for a specific type of property
     */
    private String getUpdateQueryByPropertyType(Property property){
        if (property instanceof Condo){
            return "UPDATE property SET property_name = ?, type = ?, address = ?,"
                    + " cost = ?, nber_spots = ?, rental_price = ?, property_tax_rate = ?, "
                    + "school_tax_rate = ?, condo_fee = ? WHERE property_id = "+property.getPk();
        }
        else {
            return "UPDATE property SET property_name = ?, type = ?, address = ?,"
                    + " cost = ?, nber_spots = ?, rental_price = ?, property_tax_rate = ?, "
                    + "school_tax_rate = ? WHERE property_id = "+property.getPk();
        }
    }
    
    /**
     * Gets the query of the insert depending on the type of property given
     * @param property
     * @return Update query for a specific type of property
     */
    private String getInsertQueryByPropertyType(Property property){
        if (property instanceof Condo){
            return "INSERT INTO property (insurance_id, mortgage_id, property_name, type, address,"
                    + " cost, nber_spots, rental_price, property_tax_rate, "
                    + "school_tax_rate,condo_fee) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        } else {
            return "INSERT INTO property (insurance_id, mortgage_id, property_name, type, address,"
                    + " cost, nber_spots, rental_price, property_tax_rate, "
                    + "school_tax_rate) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }
    }
    
    /**
     * Adds extra data to the prepared statement depending on the type of property
     * @param ps PreparedStatment to add values too
     * @param property property to add
     * @throws SQLException 
     */
    private void setPreparedStatementValueByPropertyType(PreparedStatement ps, Property property, int index) throws SQLException{
        if (property instanceof Condo){
            ps.setDouble(index, ((Condo)property).getCondoFee());
        }
    }
    

    

}
