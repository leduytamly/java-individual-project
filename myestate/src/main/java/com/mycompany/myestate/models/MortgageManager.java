/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Property Manager Class responsible for mortgages and the database
 * @author leduy
 */
public class MortgageManager extends AManager{
    
    /**
     * Gets all mortgages that is in the database
     * @return ObservableList<Mortgage> of all the mortgages
     */
    public ObservableList<Mortgage> loadAllMortgages() {
        ObservableList<Mortgage> mortgages = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "SELECT * FROM mortgage JOIN property USING(mortgage_id) JOIN bank_institution USING(bank_id)";
            statement = conn.prepareStatement(query);
            resultSet = statement.executeQuery(query);
            
            /*Loops through all the mortgages in the DB*/
            while (resultSet.next()){
                
                /*Creates a bank object and a String with property name*/
                BankInstitution bank = new BankInstitution(resultSet.getInt("bank_id"),resultSet.getString("institution_name"),resultSet.getDouble("interest_rate"));
                String propertyName = resultSet.getString("property_name");
                
                /*Creating a mortage object*/
                Mortgage mortgage = new Mortgage(resultSet.getInt("mortgage_id"),propertyName,
                        LocalDate.parse(resultSet.getString("payment_date")),
                        LocalDate.parse(resultSet.getString("start_date")),
                        LocalDate.parse(resultSet.getString("end_date")),
                        resultSet.getDouble("down_payment"),bank);
                /*Adding the mortgage to the ObservableList*/
                mortgages.add(mortgage);
            }
        } catch (SQLException e){
            System.out.println("ERROR LOADING MORTGAGES");
            System.out.println(e);
        } finally {
            /*Closes Connection,Statmenent, ResultSet */
            close(conn,statement,resultSet);
            System.out.println("Database Closed");
        }
        return mortgages;
    }
    
    /**
     * Updates a mortgage object in the database
     * @param mortgage Mortgage object to update
     */
    public void renewMortgage(Mortgage mortgage) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "UPDATE mortgage SET bank_id = ?, payment_date = ?, start_date = ?,"
                    + " end_date = ?, down_payment = ? WHERE mortgage_id = "+mortgage.getPk();
            System.out.println(query);
            ps = conn.prepareStatement(query);
            ps.setInt(1, mortgage.getBank().getPk());
            ps.setString(2, mortgage.getNextPaymentDate().format(DateTimeFormatter.ISO_DATE));
            ps.setString(3, mortgage.getStartDate().format(DateTimeFormatter.ISO_DATE));
            ps.setString(4, mortgage.getEndDate().format(DateTimeFormatter.ISO_DATE));
            ps.setDouble(5, mortgage.getDownPayment());
            /*Execute the update */
            ps.executeUpdate();
        } catch (SQLException e){
            System.out.println("ERROR WHEN UPDATING MORTGAGE");
            System.out.println(e);
        } finally {
            /*Closes Connection,Prepared Statement */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Inserts a mortgage into the database and gets its primary key
     * @param mortgage Mortgage Object
     * @return pk of the newly inserted mortgage 
     */
    public int  insertMortgage(Mortgage mortgage) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs =  null;
        int pk = 0;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "INSERT INTO mortgage (bank_id, payment_date, start_date, end_date, down_payment) VALUES (?,?,?,?,?)";
            System.out.println(query);
            ps = conn.prepareStatement(query);
            ps.setInt(1, mortgage.getBank().getPk());
            ps.setString(2, mortgage.getNextPaymentDate().toString());
            ps.setString(3, mortgage.getStartDate().toString());
            ps.setString(4, mortgage.getEndDate().toString());
            ps.setDouble(5, mortgage.getDownPayment());
            ps.executeUpdate();
            
            /*Gets the last inserted id*/
            pk = super.getLastId(conn, ps, rs);
            
        } catch (SQLException e) {
            System.out.println("ERROR INSRET MORTGAGE");
        } finally {
            /*Closes Connection,Statmenent, ResultSet */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
        return pk;
    }
}
