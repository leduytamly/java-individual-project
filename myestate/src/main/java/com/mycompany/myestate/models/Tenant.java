/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.time.LocalDate;
import javafx.beans.property.SimpleStringProperty;

/**
 *Class Representing a Tenant
 * @author leduy
 */
public class Tenant {
    /*All attributes for a Tenant*/
    private int pk;
    private SimpleStringProperty tenantFullName;
    private Property property;
    private SimpleStringProperty tenantPhoneNumber;
    private SimpleStringProperty tenantEmail;
    private LocalDate tenantRentStart;
    private LocalDate tenantRentEnd;
    private double tenantYearlyIncome;
    private double tenantIncomeDebtRatio;
    private boolean tenantStatus;
    private SimpleStringProperty tenantPaymentMethond;
    
    /*Lease object*/
    private Lease lease;

    /**
     * Parameterized Constructor for Tenant used when loading
     * @param pk Primary Key
     * @param tenantFullName Fullname of the Tenant
     * @param property Property Object
     * @param tenantPhoneNumber Phone Number of the Tenant
     * @param tenantEmail Email of the Tenant
     * @param tenantLeaseStart Start date of the lease
     * @param tenantLeaseEnd End date of the lease
     * @param tenantYearlyIncome Yearly Income of the Tenant
     * @param tenantIncomeDebtRatio Income/Debt Ration of the Tenant
     * @param tenantStatus Status(paid/unpaid) of the Tenant
     * @param tenantPaymentMethond  Payment method of the Tenant
     * @param lease Lease object
     */
    public Tenant(int pk, String tenantFullName, Property property, String tenantPhoneNumber, String tenantEmail, LocalDate tenantLeaseStart, LocalDate tenantLeaseEnd, double tenantYearlyIncome, double tenantIncomeDebtRatio, boolean tenantStatus, String tenantPaymentMethond, Lease lease) {
        this.pk = pk;
        this.tenantFullName = new SimpleStringProperty(tenantFullName);
        this.property= property;
        this.tenantPhoneNumber = new SimpleStringProperty(tenantPhoneNumber);
        this.tenantEmail = new SimpleStringProperty(tenantEmail);
        this.tenantRentStart = tenantLeaseStart;
        this.tenantRentEnd = tenantLeaseEnd;
        this.tenantYearlyIncome = tenantYearlyIncome;
        this.tenantIncomeDebtRatio = tenantIncomeDebtRatio;
        this.tenantStatus = tenantStatus;
        this.tenantPaymentMethond = new SimpleStringProperty(tenantPaymentMethond);
        this.lease = lease;
    }

    
    
    /**
     * Parameterized Constructor for Tenant
     * @param tenantFullName Fullname of the Tenant
     * @param property Property Object
     * @param tenantPhoneNumber Phone Number of the Tenant
     * @param tenantEmail Email of the Tenant
     * @param tenantLeaseStart Start date of the lease
     * @param tenantLeaseEnd End date of the lease
     * @param tenantYearlyIncome Yearly Income of the Tenant
     * @param tenantIncomeDebtRatio Income/Debt Ration of the Tenant
     * @param tenantStatus Status(paid/unpaid) of the Tenant
     * @param tenantPaymentMethond  Payment method of the Tenant
     * @param lease Lease object
     */
    public Tenant(String tenantFullName, Property property, String tenantPhoneNumber, String tenantEmail, LocalDate tenantLeaseStart, LocalDate tenantLeaseEnd, double tenantYearlyIncome, double tenantIncomeDebtRatio, boolean tenantStatus, String tenantPaymentMethond, Lease lease) {
        this.tenantFullName = new SimpleStringProperty(tenantFullName);
        this.property = property;
        this.tenantPhoneNumber = new SimpleStringProperty(tenantPhoneNumber);
        this.tenantEmail = new SimpleStringProperty(tenantEmail);
        this.tenantRentStart = tenantLeaseStart;
        this.tenantRentEnd = tenantLeaseEnd;
        this.tenantYearlyIncome = tenantYearlyIncome;
        this.tenantIncomeDebtRatio = tenantIncomeDebtRatio;
        this.tenantStatus = tenantStatus;
        this.tenantPaymentMethond = new SimpleStringProperty(tenantPaymentMethond);
        this.lease = lease;
   
    }
    
    /*Getters for Tenants*/
    public int getPk(){
        return pk;
    }
    
    public String getTenantFullName() {
        return tenantFullName.get();
    }

    public Property getProperty() {
        return property;
    }

    public String getTenantPhoneNumber() {
        return tenantPhoneNumber.get();
    }

    public String getTenantEmail() {
        return tenantEmail.get();
    }

    public LocalDate getTenantRentStart() {
        return tenantRentStart;
    }

    public LocalDate getTenantRentEnd() {
        return tenantRentEnd;
    }

    public double getTenantYearlyIncome() {
        return tenantYearlyIncome;
    }

    public double getTenantIncomeDebtRatio() {
        return tenantIncomeDebtRatio;
    }

    public boolean getTenantStatus() {
        return tenantStatus;
    }

    public String getTenantPaymentMethond() {
        return tenantPaymentMethond.get();
    }
    
    public Lease getLease(){
        return lease;
    }

    /*Setters for Tenant*/
    public void setTenantFullName(String tenantFullName) {
        this.tenantFullName = new SimpleStringProperty(tenantFullName);
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public void setTenantPhoneNumber(String tenantPhoneNumber) {
        this.tenantPhoneNumber = new SimpleStringProperty(tenantPhoneNumber);
    }

    public void setTenantEmail(String tenantEmail) {
        this.tenantEmail = new SimpleStringProperty(tenantEmail);
    }

    public void setTenantRentStart(LocalDate tenantLeaseStart) {
        this.tenantRentStart = tenantLeaseStart;
    }

    public void setTenantRentEnd(LocalDate tenantLeaseEnd) {
        this.tenantRentEnd = tenantLeaseEnd;
    }

    public void setTenantYearlyIncome(double tenantYearlyIncome) {
        this.tenantYearlyIncome = tenantYearlyIncome;
    }

    public void setTenantIncomeDebtRation(double tenantIncomeDebtRatio) {
        this.tenantIncomeDebtRatio = tenantIncomeDebtRatio;
    }

    public void setTenantStatus(boolean tenantStatus) {
        this.tenantStatus = tenantStatus;
    }

    public void setTenantPaymentMethond(String tenantPaymentMethond) {
        this.tenantPaymentMethond = new SimpleStringProperty(tenantPaymentMethond);
    }
    
    public void setLease(Lease lease){
        this.lease = lease;
    }
}
