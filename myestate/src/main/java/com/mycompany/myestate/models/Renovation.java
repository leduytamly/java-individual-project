/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import javafx.beans.property.SimpleStringProperty;

/**
 *Class Representing a Renovation
 * @author leduy
 */
public class Renovation {
    /*All Attributes for a Renovation*/
    private int pk;
    private Contractor contractor;
    private Property property;
    private SimpleStringProperty renovationType;
    private SimpleStringProperty renovationDescription;
    private double renovationCost;
    private boolean isFinished;
    
    /**
     * Parameterized Constructor for Renovation
     * @param pk Primary key of the renovation
     * @param contractor Contractor object
     * @param property Property object
     * @param renovationType Type of Renovation
     * @param renovationDescription Description of the Renovation
     * @param renovationCost Cost of the Renovation
     * @param isFinished renovation finished or not 
     */
    public Renovation(int pk, Contractor contractor, Property property, String renovationType, String renovationDescription, double renovationCost, boolean isFinished) {
        this.pk = pk;
        this.contractor = contractor;
        this.property = property;
        this.renovationType = new SimpleStringProperty(renovationType);
        this.renovationDescription = new SimpleStringProperty(renovationDescription);
        this.renovationCost = renovationCost;
        this.isFinished = isFinished;
    }
    
     /**
     * Parameterized Constructor for Renovation
     * @param contractor Contractor object
     * @param property Property object
     * @param renovationType Type of Renovation
     * @param renovationDescription Description of the Renovation
     * @param renovationCost Cost of the Renovation
     * @param isFinished renovation finished or not 
     */
    public Renovation(Contractor contractor, Property property, String renovationType, String renovationDescription, double renovationCost, boolean isFinished) {
        this.contractor = contractor;
        this.property = property;
        this.renovationType = new SimpleStringProperty(renovationType);
        this.renovationDescription = new SimpleStringProperty(renovationDescription);
        this.renovationCost = renovationCost;
        this.isFinished = isFinished;
    }
    
    /*Getters for Renovation*/
    public int getPk(){
        return pk;
    }
    
    public Contractor getContractor() {
        return contractor;
    }

    public Property getProperty() {
        return property;
    }

    public String getRenovationType() {
        return renovationType.get();
    }

    public String getRenovationDescription() {
        return renovationDescription.get();
    }

    public double getRenovationCost() {
        return renovationCost;
    }

    public boolean getRenovationStatus() {
        return isFinished;
    }
    
    /*Setters for Renovation*/
    public void setContractor(Contractor contractor) {
        this.contractor = contractor;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public void setRenovationType(String renovationType) {
        this.renovationType = new SimpleStringProperty(renovationType);
    }

    public void setRenovationDescription(String renovationDescription) {
        this.renovationDescription = new SimpleStringProperty(renovationDescription);
    }

    public void setRenovationCost(double renovationCost) {
        this.renovationCost = renovationCost;
    }

    public void setRenovationStatus(boolean isFinished) {
        this.isFinished = isFinished;
    }
}
