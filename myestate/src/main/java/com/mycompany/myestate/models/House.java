/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

/**
 * House Object
 * @author leduy
 */
public class House extends Property {
    
    /**
     * Parameterized Constructor for House (Used when loading from the database)
     * @param pk Primary Key of the House   
     * @param propertyName Name of the property
     * @param propertyAddress Address of the property
     * @param propertyCost Cost of the property
     * @param propertyRentalPrice Rental price of the property
     * @param propertyTaxRate Tax rate of the property
     * @param propertySchoolTaxRate Shool Tax Rate of the property
     * @param insurance Insurance of the property
     */
    public House(int pk, String propertyName, String propertyAddress, double propertyCost, double propertyRentalPrice, double propertyTaxRate, double propertySchoolTaxRate, Insurance insurance) {
        super(pk, propertyName, "House", propertyAddress, propertyCost, 1, propertyRentalPrice, propertyTaxRate, propertySchoolTaxRate, insurance);
    }
    
    /**
     * Parameterized Constructor for House (Used when creating a house)
     * @param propertyName Name of the property
     * @param propertyAddress Address of the property
     * @param propertyCost Cost of the property
     * @param propertyRentalPrice Rental price of the property
     * @param propertyTaxRate Tax rate of the property
     * @param propertySchoolTaxRate Shool Tax Rate of the property
     * @param insurance Insurance of the property
     */
    public House(String propertyName, String propertyAddress, double propertyCost, double propertyRentalPrice, double propertyTaxRate, double propertySchoolTaxRate, Insurance insurance) {
        super(propertyName, "House", propertyAddress, propertyCost, 1, propertyRentalPrice, propertyTaxRate, propertySchoolTaxRate, insurance);
    }
    
}
