/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Renovation Manager Class responsible for renovations and the database 
 * @author leduy
 */
public class RenovationManager extends AManager {
    
    private PropertyManager pm = new PropertyManager();
    
    /**
     * Gets all Renovations that is in the database
     * @return ObservableList<Renovation> with all properties
     */
    public ObservableList<Renovation> loadAllRenovations() {
        ObservableList<Renovation> renovations = FXCollections.observableArrayList();
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "SELECT * FROM renovation JOIN property USING(property_id) JOIN contractor USING(contractor_id)";
            statement = conn.prepareStatement(query);
            rs = statement.executeQuery(query);
            
            /*Loops through all properties in the DB*/
            while (rs.next()){
                
                /*Creates an contractor object*/
                Contractor contractor = new Contractor(rs.getInt("contractor_id"),rs.getString("contractor_name"),
                        rs.getString("company"),rs.getString("phone_number"),rs.getString("email"),rs.getString("profession"));
                
                /*Creating the property object*/
                Property property = pm.createPropertyByType(rs.getString("type"),rs,null);
                
                Renovation renovation = new Renovation(rs.getInt("renovation_id"),contractor,property,
                        rs.getString("renovation_type"),rs.getString("description"),rs.getDouble("cost"),
                        rs.getBoolean("isFinished"));
                
                /*Adding the property to the ObservableList*/
                renovations.add(renovation);
            }
        } catch (SQLException e){
            System.out.println("ERROR LOADING Renovations");
            System.out.println(e);
        } finally {
            /*Closes Connection,Statmenent, ResultSet */
            close(conn,statement,rs);
            System.out.println("Database Closed");
        }
        /*Returning the ObservableList*/
        return renovations;
    }
    
    /**
     * Updates a renovation object in the database
     * @param renovation Renovation Object to update in database
     */
    public void updateRenovation(Renovation renovation) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "UPDATE renovation SET contractor_id = ?, property_id = ?, renovation_type = ?,"
                    + " description = ?, cost = ?, isFinished = ? WHERE renovation_id = "+renovation.getPk();
            System.out.println(query);
            ps = conn.prepareStatement(query);
            ps.setInt(1, renovation.getContractor().getPk());
            ps.setInt(2, renovation.getProperty().getPk());
            ps.setString(3, renovation.getRenovationType());
            ps.setString(4, renovation.getRenovationDescription());
            ps.setDouble(5, renovation.getRenovationCost());
            ps.setBoolean(6, renovation.getRenovationStatus());
            
            /*Execute the update */
            ps.executeUpdate();
            
        } catch (SQLException e){
            System.out.println("ERROR WHEN UPDATING RENOVATION");
            System.out.println(e);
        } finally {
            /*Closes Connection,Prepared Statement */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Inserts a new renovation in the database
     * @param renovation
     */
    public void insertRenovation(Renovation renovation) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "INSERT INTO renovation (contractor_id, property_id, renovation_type, description, cost)"
                    + "VALUES (?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1,renovation.getContractor().getPk());
            ps.setInt(2, renovation.getProperty().getPk());
            ps.setString(3,renovation.getRenovationType());
            ps.setString(4, renovation.getRenovationDescription());
            ps.setDouble(5, renovation.getRenovationCost());
            
            /*Executing update*/
            ps.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println("ERROR WHEN INSERTING RENOVATION");
            System.out.println(e);
        } finally {
            /*Closes Connection,Statmenent */
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
    
    /**
     * Deletes a property from the database
     * @param renovation Renovation Object to delete
     */
    public void deleteRenovation(Renovation renovation) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            /*Making the connection*/
            conn =  super.getConnection();
            /*Making the statement and executing the query*/
            String query = "DELETE FROM renovation WHERE renovation_id = \""+renovation.getPk()+"\"";
            System.out.println(query);
            /*Creating the prepared statement and executing the delete*/
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch(SQLException e){
            System.out.println("ERROR WHEN DELETING Renovation");
            System.out.println(e);
        } finally {
            /*Closing Connection and Prepared Statement*/
            close(conn);
            close(ps);
            System.out.println("Database Closed");
        }
    }
}
