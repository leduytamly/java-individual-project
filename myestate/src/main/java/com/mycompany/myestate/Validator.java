/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

/**
 * Class to validate inputs
 * @author leduy
 */
public class Validator {
    
    /**
     * Validates if the text input is a decimal number
     * @param s String to validate
     * @return Boolean value representing if the string is a decimal number
     */
    public static boolean isDecimalNumber(String s){
        return s.matches("\\d{0,}([\\.]\\d{0,})?");
    }
    
    /**
     * Validates if the text input is a valid email
     * @param s String to validate
     * @return Boolean value representing if the string is a valid email
     */
    public static boolean isValidEmail(String s){
        return s.matches("[^@ \\t\\r\\n]+@[^@ \\t\\r\\n]+\\.[a-z]{1,}[^@ \\t\\r\\n]+");
    }
    
    /**
     * Validates if the text input is a valid phone number
     * @param s String to validate
     * @return Boolean value representing if the string is a valid phone number
     */
    public static boolean isValidPhoneNumber(String s){
        return s.matches("^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$");
    }
}
