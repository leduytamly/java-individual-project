/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *Controller Abstract Class
 * Holds methods that all classes have
 * @author leduy
 */
public abstract class AController{
    /*Label for error*/
    @FXML Label error;
    /*Validity of the form*/
    protected boolean isFormValid = true;
    
    /**
     * Loads a FXML file
     * @param fxml Filename of the FXML file
     * @return FXMLLoader Object
     * @throws IOException 
     */
    protected FXMLLoader loadFXML(String fxml) throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource(fxml+".fxml"));
        return loader;
    }
    
    /**
     * Shows a scene
     * @param event
     * @param scene  Scene to be shown
     */
    protected void showScene(ActionEvent event,Scene scene){
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(scene);
        /*Confirmation before closing the application*/
        window.setOnCloseRequest(e -> {
            e.consume();
            boolean close = false;
            try {
                close = switchConfirmationBox("Are you sure you want to close the application? Unsaved data will be lost");
            } catch (IOException ex) {
                System.out.println(ex);
            }
            
            if (close){
                window.close();
            } 
        });
        window.show();
    }
    
    /**
     * Switch the scene to the main properties view
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void switchToProperties(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("properties");
        Scene propertiesScene = new Scene(loader.load());
        showScene(event,propertiesScene);
    }
    
    /**
     * Switch the scene to the main mortgages view
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void switchToMortgages(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("mortgages");
        Scene mortgagesScene = new Scene(loader.load());
        showScene(event,mortgagesScene);
    }
    
    /**
     * Switch the scene the main tenants view
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void switchToTenants(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("tenants");
        Scene tenantsScene = new Scene(loader.load());
        showScene(event,tenantsScene);
    }
    
    /**
     * Switch the scene to the main contractor view
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void switchToContractors(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("contractors");
        Scene contractorsScene = new Scene(loader.load());
        showScene(event,contractorsScene);
    }
    
    /**
     * Switch the scene to the main Renovation view
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void switchToRenovations(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("renovations");
        Scene renovationsScene = new Scene(loader.load());
        showScene(event,renovationsScene);
    }
    
    /**
     * Opens a new window for Confirmation Box view
     * @param message Message
     * @return If the user confirms or cancels
     * @throws IOException 
     */
    protected boolean switchConfirmationBox(String message) throws IOException {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(null);
        alert.setContentText(message);
        Optional <ButtonType> action = alert.showAndWait();
        return action.get() == ButtonType.OK;
    }
    
    /**
     * Opens a new window for Warnings
     * @param message warning message 
     * @throws IOException 
     */
    protected void switchWarningBox(String message) throws IOException {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
    
    /**
     * Displays an error message to the user
     * @param message Message to display in red
     */
    protected void displayError(String message){
        error.setText(message);
    }
    
    /**
     * Validates if the values entered is a digit or not
     * @param textFields 
     */
    protected void validateNumbers(HashSet<TextField> textFields){
        textFields.forEach(item -> {
            
            item.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    if (!Validator.isDecimalNumber(newValue)) {
                        item.setText(newValue.replace(newValue, oldValue));
                        error.setText("Value must be a number");
                    } else {
                        error.setText("");
                    }
                }
            });
            
        });
    }
    
    /**
     * Validates email field if they are valid or not
     * @param textFields Email TextFields to validate
     */
    protected void validateEmail(HashSet<TextField> textFields){
        textFields.forEach(item -> {
            
            item.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) -> {
                if (!newValue){
                    if (!Validator.isValidEmail(item.getText())) {
                        isFormValid = false;
                        item.setStyle("-fx-text-box-border: red");
                        error.setText("Invalid email");
                    } else {
                        isFormValid = true;
                        item.setStyle(null);
                        error.setText("");
                    } 
                }
            });
            
        });
    }
    
    /**
     * Validates Phone number field if they are valid or not
     * @param textFields Phone Number TextFields to validate
     */
    protected void validatePhoneNumber(HashSet<TextField> textFields){
        textFields.forEach(item -> {
            
            item.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) -> {
                if (!newValue){
                    if (!Validator.isValidPhoneNumber(item.getText())) {
                        item.setStyle("-fx-text-box-border: red");
                        error.setText("Invalid phone number");
                    } else {
                        item.setStyle(null);
                        error.setText("");
                    } 
                }
            });
            
        });
    }
    
    /**
     * Validates if a TextField in a HashSet is empty
     * @param textFields TextFields to validate
     */
    protected void validateEmptyTextField(HashSet<TextField> textFields){
        for (TextField tf : textFields){
            if (tf.getText().trim().isEmpty()){
                isFormValid = false;
                tf.setStyle("-fx-text-box-border: red");
            } else {
                tf.setStyle(null);
            }
        }
    }
    
    /**
     * Validates if a TextArea in a HashSet is empty
     * @param textAreas TextAreas to validate
     */
    protected void validateEmptyTextArea(HashSet<TextArea> textAreas){
        for (TextArea ta : textAreas){
            if (ta.getText().trim().isEmpty()){
                isFormValid = false;
                ta.setStyle("-fx-text-box-border: red"); 
            } else {
                ta.setStyle("null");
            }
        }
    }
    
    /**
     * Validates if a DatePicker in a HashSet is empty
     * @param dateFields DatePickers to validate
     */
    protected void validateEmptyDateField(HashSet<DatePicker> dateFields){
        for (DatePicker dp : dateFields){
            if (dp.getValue() == null){
                isFormValid = false;
                dp.setStyle("-fx-border-color: red");
            } else{
                dp.setStyle(null);
            }
        }
    }
    
    /**
     * Validates if a Choice Box is empty
     * @param choiceboxes ChoiceBox to validate
     */
    protected void validateEmptyChoiceBox(HashSet<ChoiceBox> choiceboxes){
        for (ChoiceBox cb : choiceboxes){
            if (cb.getValue() == null){
                isFormValid = false;
                cb.setStyle("-fx-border-color: red");
            } else{
                cb.setStyle(null);
            }
        }
    }
    
    /**
     * Checks if the form is valid
     */
    protected void isValid(){
        if (!this.isFormValid){
            /*Resets the form to true*/
            this.isFormValid = true;
            throw new IllegalArgumentException("Empty Fields");
        } 
    }
            
}
