/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Contractor;
import com.mycompany.myestate.models.ContractorManager;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * Controller For Edit Contractor View
 * @author leduy
 */
public class EditContractorController extends AContractorController{
    
    /*Contractor Object*/
    private Contractor selectedContractor;
    
    /*FXML id for the header*/
    @FXML private Label header;
    
    /**
     * Initializes the view
     */
    @FXML
    private void initialize(){
        super.setAllListeners();
    }
    
    /**
     * Initializes Edit Contractor view with the inputs pre loaded
     * @param contractor Contractor object to pre load
     */
    public void initializeContractor(Contractor contractor){
        selectedContractor = contractor;
        contractorName.setText(selectedContractor.getContractorName());
        contractorCompanyName.setText(selectedContractor.getContractorCompany());
        contractorPhoneNumber.setText(selectedContractor.getContractorPhoneNumber());
        contractorEmail.setText(selectedContractor.getContractorEmail());
        contractorProfession.setText(selectedContractor.getContractorProfession());
    }
    
    /**
     * Disables all inputs for the purpose of viewing only
     */
    public void setReadOnly(){
        //Changin the header
        header.setText("Overview of a Contractor");
        
        //Disables all input fields
        contractorName.setDisable(true);
        contractorCompanyName.setDisable(true);
        contractorPhoneNumber.setDisable(true);
        contractorEmail.setDisable(true);
        contractorProfession.setDisable(true);
    }
    
    /**
     * Sets the selectedContractor with new values
     */
    private void setContractor(){
        selectedContractor.setContractorName(contractorName.getText());
        selectedContractor.setContractorCompany(contractorCompanyName.getText());
        selectedContractor.setContractorPhoneNumber(contractorPhoneNumber.getText());
        selectedContractor.setContractorEmail(contractorEmail.getText());
        selectedContractor.setContractorProfession(contractorProfession.getText());
    }
    
    /**
     * Makes the update in the database
     * @param event
     * @throws IOException
     */
    @FXML
    private void makeChange(ActionEvent event) throws IOException {
        if (switchConfirmationBox("Are you sure you want to make the change?")){
            try{
                /*Data validation*/
                super.validateAllFields();
                
                /*Sets the contractor with new values and execute the update*/
                setContractor();
                ContractorManager cm = new ContractorManager();
                cm.updateContractor(selectedContractor);
                
                /*Change the back to contractor*/
                switchToContractors(event);
                
            } catch (IOException e) {
                System.out.println(e);
                System.out.println("ERROR SHOWING WARNING");
            } catch(IllegalArgumentException e) {
                System.out.println(e);
                switchWarningBox("Missing values!");
            }
        }
    }
}
