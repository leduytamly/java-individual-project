/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.exceptions;

/**
 * Exception for when a property reached its max number of tenants
 * @author leduy
 */
public class PropertyIsFullException extends Exception{
    public PropertyIsFullException(String message){
        super(message);
    }   
}