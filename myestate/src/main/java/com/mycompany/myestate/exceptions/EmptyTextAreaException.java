/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate.exceptions;

/**
 * Exception for when a Text Area is empty
 * @author leduy
 */
public class EmptyTextAreaException extends Exception{
    public EmptyTextAreaException(String message){
        super(message);
    }
}

