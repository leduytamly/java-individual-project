/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.BankInstitution;
import com.mycompany.myestate.models.BankInstitutionManager;
import com.mycompany.myestate.models.Mortgage;
import com.mycompany.myestate.models.MortgageManager;
import com.mycompany.myestate.models.Property;
import com.mycompany.myestate.models.PropertyManager;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

/**
 *Controller Class For New Mortgage View
 * @author leduy
 */
public class NewMortgageController extends AMortgageController{
    
    /*Property Object*/
    private Property selectedProperty;
    /*Mortgage Object TODO*/
    
    /**
     * initializes the scene (the choiceBox)
     */
    @FXML
    private void initialize() {
        BankInstitutionManager bim = new BankInstitutionManager();
        bank.setItems(bim.loadAllBanks());
        
        super.setAllListeners();
    }
    
    /**
     * Switch the view to New Property
     * (If user wants got back)
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToNewProperty(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("newproperty");
        Scene newPropertyScene = new Scene(loader.load());
        
        //Gets the controller of the new newmortgage View
        NewPropertyController controller = loader.getController();
        //Initializes the data of the new view with the new property object
        controller.initializeProperty(selectedProperty);
        
        showScene(event,newPropertyScene);
    }
    
    /**
     * Initializes the property name label of the newly created property
     * @param property 
     */
    public void initializeProperty (Property property){
        selectedProperty = property;
        propertyName.setText(selectedProperty.getPropertyName());
    }
    

    /**
     * Creates a mortgage object with the input
     * @return Mortgage object
     */
    private Mortgage createMortgage(){
        Mortgage mortgage = new Mortgage(
                selectedProperty.getPropertyName(),
                nextPaymentDate.getValue(),
                startDate.getValue(),
                endDate.getValue(),
                Double.parseDouble(downPayment.getText()),
                (BankInstitution)bank.getValue()
        );
        return mortgage;
    }
    
    /**
     * Switch the scene to the main bank view which passes a property object
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void switchToBanks(ActionEvent event) throws IOException {
        FXMLLoader loader = super.loadFXML("banks");
        Scene bankScene = new Scene(loader.load());
        
        //Gets the controller of the bank View
        BankController controller = loader.getController();

       try {
            /*Sends a property to the bank view*/
            controller.initializeProperty(selectedProperty);
            super.showScene(event,bankScene);
        } catch (IllegalArgumentException e) {
            System.out.println(e);
            switchWarningBox("Missing values!");
        }
    }
    
    /**
     * Makes the inserts into the database
     * @throws IOException 
     */
    @FXML
    private void makeInserts(ActionEvent event) throws IOException{
        try {
            /*Data validation*/
            super.validateAllFields();
            
            MortgageManager mm = new MortgageManager();
            PropertyManager pc = new PropertyManager();
            int pkMortgage = mm.insertMortgage(createMortgage());
            int pkInsurance = pc.insertInsurance(selectedProperty.getInsurance());
            pc.insertProperty(selectedProperty, pkInsurance, pkMortgage);
            switchToProperties(event);
        } catch (IOException e) {
                System.out.println(e);
                System.out.println("ERROR SHOWING WARNING");
        } catch(IllegalArgumentException e) {
            System.out.println(e);
            switchWarningBox("Missing values!");
        }
    }
}
