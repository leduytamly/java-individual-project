/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.BankInstitution;
import com.mycompany.myestate.models.Mortgage;
import com.mycompany.myestate.models.MortgageManager;
import java.io.IOException;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *Controller Class for Mortgage View 
 * @author leduy
 */
public class MortgageController extends AController{
    
    /*FXML ids for the table and columns*/
    @FXML private TableView<Mortgage> tableView;
    @FXML private TableColumn<Mortgage,String> propertyNameCol;
    @FXML private TableColumn<Mortgage,BankInstitution> bankNameCol;
    @FXML private TableColumn<Mortgage,LocalDate> nextPaymentCol;
    @FXML private TableColumn<Mortgage,Integer> downPaymentCol;
    
    /**
     * Switch the view to Renew Mortgage
     * It will pass in a Mortgage Object to pre load in the form
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToRenewMortgage(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("renewmortgage");
        Scene detailedPropertyScene = new Scene(loader.load());
        
        try{
        //Gets the controller of the renew mortgage View
        RenewMortgageController controller = loader.getController();
        //Initializes the data of the new view with the selected property object
        controller.initializeMortgage(tableView.getSelectionModel().getSelectedItem());
        
        showScene(event,detailedPropertyScene);
        } catch (NullPointerException e) {
            super.displayError("You must select a Mortgage first!");
        }
    }
    
    /**
     * Switch the view to Detailed Mortgage
     * It will pass in a Mortgage object for the user to view
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToDetailedMortgage(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("renewmortgage");
        Scene detailedPropertyScene = new Scene(loader.load());
       
       try { 
        //Gets the controller of the renew mortgage View
        RenewMortgageController controller = loader.getController();
        //Initializes the data of the new view with the selected property object
        controller.initializeMortgage(tableView.getSelectionModel().getSelectedItem());
        //Sets the view to read only (for viewing only)
        controller.setReadOnly();
        showScene(event,detailedPropertyScene);
        } catch (NullPointerException e) {
            super.displayError("You must select a Mortgage first!");
        }
    }
    
    /**
     * Initializes the TableView with data
     */
    @FXML
    private void initialize() {
        /*Setup columns*/
        propertyNameCol.setCellValueFactory(new PropertyValueFactory<Mortgage,String>("propertyName"));
        bankNameCol.setCellValueFactory(new PropertyValueFactory<Mortgage,BankInstitution>("bank"));
        nextPaymentCol.setCellValueFactory(new PropertyValueFactory<Mortgage,LocalDate>("nextPaymentDate"));
        downPaymentCol.setCellValueFactory(new PropertyValueFactory<Mortgage,Integer>("downPayment"));
        
        //Load test data
        tableView.setItems(getMortgages());
    }
    
    /**
     * Gets all Mortgages
     * @return ObservervableList of Mortgages
     */
    private ObservableList<Mortgage> getMortgages() {
        //Test Data (Load all mortgages from the database)
        ObservableList<Mortgage> mortgages = FXCollections.observableArrayList();
        MortgageManager mm = new MortgageManager();
        mortgages = mm.loadAllMortgages();
        return mortgages;
    }
    
}
