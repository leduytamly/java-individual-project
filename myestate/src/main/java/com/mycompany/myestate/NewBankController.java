/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.BankInstitution;
import com.mycompany.myestate.models.BankInstitutionManager;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 * Controller for new bank view
 * @author leduy
 */
public class NewBankController extends ABankController{
    
    /**
     * Initialize the view
     */
    @FXML
    private void initialize(){
        super.setAllListeners();
    }
    
    /**
     * Creates the bank object with the values in the input
     * @return Newly Created Bank Object
     */
    private BankInstitution createBank(){
        BankInstitution bank = new BankInstitution(bankName.getText(),Double.parseDouble(interestRate.getText()));
        return bank;
    }
    
    /**
     * Inserts contractor into the database
     * @param event
     * @throws IOException 
     */
    @FXML
    private void makeInserts(ActionEvent event) throws IOException{
        try{
            /*Data validation*/
            super.validateAllFields();
            
            /*Creates the contractor and inserts it into the database*/
            BankInstitutionManager bim = new BankInstitutionManager();
            bim.insertBank(createBank());
            
            /*Switches the view back to Contractors*/
            super.switchToBanks(event);
            
        } catch (IOException e) {
            System.out.println(e);
            System.out.println("ERROR SHOWING WARNING");
        } catch(IllegalArgumentException e) {
            System.out.println(e);
            switchWarningBox("Missing values!");
        }
    }
}
