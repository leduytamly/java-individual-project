/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.BankInstitution;
import com.mycompany.myestate.models.BankInstitutionManager;
import com.mycompany.myestate.models.Mortgage;
import com.mycompany.myestate.models.MortgageManager;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;

/**
 *Controller Class for Renew Mortgage View
 * @author leduy
 */
public class RenewMortgageController extends AMortgageController{
    
    /*Mortgage Object*/
    Mortgage selectedMortgage;
    
    /*FXML id for the header*/
    @FXML private Label header;
    
    /**
     * Initializes the Renew mortgage view
     */
    @FXML 
    public void initialize(){
        super.setAllListeners();
    }
    
    /**
     * Initializes Edit Mortgage view with the inputs pre loaded
     * @param mortgage Mortgage object to pre load
     */
    public void initializeMortgage(Mortgage mortgage) {
        selectedMortgage = mortgage;
        propertyName.setText(selectedMortgage.getPropertyName());
        nextPaymentDate.setValue(selectedMortgage.getNextPaymentDate());
        startDate.setValue(selectedMortgage.getStartDate());
        endDate.setValue(selectedMortgage.getEndDate());
        downPayment.setText(Double.toString(selectedMortgage.getDownPayment()));
        initializeChoiceBox(selectedMortgage);
    }
    
    /**
     * Initialize the choice box with the pre defined banks in the db
     * Sets the current bank of the selectedMortgage
     * @param mortgage 
     */
    public void initializeChoiceBox(Mortgage mortgage) {
        BankInstitutionManager bim = new BankInstitutionManager();
        bank.setItems(bim.loadAllBanks());
        bank.setValue(mortgage.getBank());
    }
    
    /**
     * Disables all inputs for the purpose of viewing only
     */
    public void setReadOnly(){
        //Changes the header
        header.setText("Overview of a Mortgage");
        
        //Disables all the input fields
        propertyName.setDisable(true);
        bank.setDisable(true);
        nextPaymentDate.setDisable(true);
        startDate.setDisable(true);
        endDate.setDisable(true);
        downPayment.setDisable(true);
    }
    
    /**
     * Sets the new values for the selectedMortgages
     */
    public void setNewMortgage(){
        selectedMortgage.setBank((BankInstitution)bank.getValue());
        selectedMortgage.setNextPaymentDate(nextPaymentDate.getValue());
        selectedMortgage.setStartDate(startDate.getValue());
        selectedMortgage.setEndDate(endDate.getValue());
        selectedMortgage.setDownPayment(Double.parseDouble(downPayment.getText()));
        System.out.println(selectedMortgage.getBank());
    }
    
    /**
     * Switch the scene to main bank view which passes a mortgage object
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void switchToBanks(ActionEvent event) throws IOException {
        FXMLLoader loader = super.loadFXML("banks");
        Scene bankScene = new Scene(loader.load());
        
        //Gets the controller of the bank View
        BankController controller = loader.getController();

       try {
            /*Sends a mortgage to the bank view*/
            controller.initializeMortgage(this.selectedMortgage);
            super.showScene(event,bankScene);
        } catch (IllegalArgumentException e) {
            System.out.println(e);
            switchWarningBox("Missing values!");
        }
    }
    
    /**
     * Updates the table with new values
     * @param event
     * @throws IOException 
     */
    @FXML
    private void makeChange(ActionEvent event) throws IOException{
        if (switchConfirmationBox("Are you sure you want to make the change?")){
            try {
                /*Data validation*/
                super.validateAllFields();
                /*Sets new values and updates the database*/
                setNewMortgage();
                MortgageManager mortgageManager = new MortgageManager();
                mortgageManager.renewMortgage(selectedMortgage);
                /*Switches the view to mortgages*/
                switchToMortgages(event);
            } catch (IOException e) {
                System.out.println(e);
                System.out.println("ERROR SHOWING WARNING");
            } catch(IllegalArgumentException e) {
                System.out.println(e);
                switchWarningBox("Missing values!");
            }
            
        }
    }
    
    
}
