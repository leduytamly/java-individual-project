/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Condo;
import com.mycompany.myestate.models.Insurance;
import com.mycompany.myestate.models.Property;
import com.mycompany.myestate.models.PropertyManager;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *Controller Class for Edit Property View
 * @author leduy
 */
public class EditPropertyController extends APropertyController{
    
    /*Property Object*/
    private Property selectedProperty;
    
    /*Heading Label*/
    @FXML private Label header;
    /*Confirm Button*/
    @FXML private Button confirmBtn;
    
    /**
     * Initializes the view
     */
    @FXML
    private void initialize(){
        super.initializeChoiceBox();
        super.setAllListeners();
        propertyType.setDisable(true);
    }
    
    /**
     * Initializes (loads) the inputs to reflect the property object
     * @param property Selected Property Object
     */
    public void initializeProperty (Property property){
        selectedProperty = property;
        
        /*Preloads all the data common to all property*/
        propertyName.setText(selectedProperty.getPropertyName());
        propertyType.setValue(selectedProperty.getPropertyType());
        propertyAddress.setText(selectedProperty.getPropertyAddress());
        propertyCost.setText(String.valueOf(selectedProperty.getPropertyCost()));
        propertyDoors.setText(Integer.toString(selectedProperty.getPropertyDoors()));
        propertyRentalPrice.setText(String.valueOf(selectedProperty.getPropertyRentalPrice()));
        propertyTaxRate.setText(String.valueOf(selectedProperty.getPropertyTaxRate()));
        propertySchoolTaxRate.setText(String.valueOf(selectedProperty.getPropertySchoolTaxRate()));
        insuranceCompany.setText(selectedProperty.getInsurance().getInsuranceName());
        intrestRate.setText(String.valueOf(selectedProperty.getInsurance().getInsuranceAmount()));
        
        /*Initialize extra data depending on property type*/
        initializSpecificProperty(selectedProperty);
   
    }
    
    /**
     * Changes all input fields to be non editable for when a user wants to view
     * a property (Read Only)
     */
    public void setReadOnly(){
        /*Changes the header*/
        header.setText("Detailed View of a Property");
        
        /*Disables all inputs*/
        propertyName.setDisable(true);
        propertyType.setDisable(true);
        propertyAddress.setDisable(true);
        propertyCost.setDisable(true);
        propertyDoors.setDisable(true);
        propertyRentalPrice.setDisable(true);
        propertyTaxRate.setDisable(true);
        propertySchoolTaxRate.setDisable(true);
        insuranceCompany.setDisable(true);
        intrestRate.setDisable(true);
        confirmBtn.setVisible(false);
    }

    /**
     * Sets the new values for the selectedProperty Object (common attributes)
     */
    private void setProperty(){
        selectedProperty.setPropertyName(propertyName.getText());
        selectedProperty.setPropertyAddress(propertyAddress.getText());
        selectedProperty.setPropertyCost(Double.parseDouble(propertyCost.getText()));
        selectedProperty.setPropertyDoors(Integer.parseInt(propertyDoors.getText()));
        selectedProperty.setPropertyRentalPrice(Double.parseDouble(propertyRentalPrice.getText()));
        selectedProperty.setPropertyTaxRate(Double.parseDouble(propertyTaxRate.getText()));
        selectedProperty.setPropertySchoolTaxRate(Double.parseDouble(propertySchoolTaxRate.getText()));
        
        /*Sets additional information depending on the property type*/
        setSpecificProperty();
    }
    
    /**
     * Sets the new values for specific property types
     */
    private void setSpecificProperty(){
        if (selectedProperty instanceof Condo){
            ((Condo)selectedProperty).setCondoFee(Double.parseDouble(condoFee.getText()));
        } 
    }
    
    /**
     * Sets the new values for selectedProperty's insurance object 
     */
    private void setInsurace(){
        Insurance insurance = selectedProperty.getInsurance();
        
        insurance.setInsuranceName(insuranceCompany.getText());
        insurance.setInsuranceAmount(Double.parseDouble(intrestRate.getText()));
        
        selectedProperty.setInsurance(insurance);
    }
    
    /**
     * Confirm the changes to a property
     * @param event
     */
    @FXML
    private void makeChange(ActionEvent event) throws IOException{
        if (switchConfirmationBox("Are you sure you want to make the change?")){
            try {
                /*Data validation*/
                super.validateAllFields();
                
                /*Sets the new values*/
                setProperty();
                setInsurace();
                
                /*Creates property manager and updates the property and insurance in the database*/
                PropertyManager propertyManager = new PropertyManager();
                propertyManager.updateProperty(selectedProperty);
                propertyManager.updateInsurance(selectedProperty.getInsurance());
                switchToProperties(event);
                
            } catch (IOException e) {
                System.out.println(e);
                System.out.println("ERROR SHOWING WARNING");
            } catch (IllegalArgumentException e) {
                System.out.println(e);
                switchWarningBox("Missing Values!");
            }
        }
    }
}
