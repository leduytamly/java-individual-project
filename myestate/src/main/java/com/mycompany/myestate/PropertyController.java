/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Property;
import com.mycompany.myestate.models.PropertyManager;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Controller Class for Property View
 * @author leduy
 */
public class PropertyController extends AController{
    
    /*FXML ids for the table and columns*/
    @FXML private TableView<Property> tableView;
    @FXML private TableColumn<Property,String> propertyNameCol;
    @FXML private TableColumn<Property,String> propertyTypeCol;
    @FXML private TableColumn<Property,String> propertyAddressCol;
    @FXML private TableColumn<Property,Integer> propertyCostCol;
    @FXML private TableColumn<Property,Integer> propertyDoorsCol;
    
    /*Label for errors*/
    @FXML private Label error;
    
    /**
     * Switch the scene to new properties
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToNewProperty(ActionEvent event) throws IOException {
        FXMLLoader loader = super.loadFXML("newproperty");
        Scene newPropertyScene = new Scene(loader.load());
        super.showScene(event,newPropertyScene);
    }
    
    /**
     * Deletes a property, Displays a confirmation box before
     * @throws IOException 
     */
    @FXML
    private void deleteProperty() throws IOException {
        PropertyManager pm = new PropertyManager();
        try{
            Property property = tableView.getSelectionModel().getSelectedItem();
            if (property == null){
                throw new NullPointerException("no property selected");
            }
            if (switchConfirmationBox("Are you sure you want to delete? This cannot be undone!")){
                pm.deleteProperty(property);
                initialize();
            } 
        } catch (NullPointerException e){
            super.displayError("You must select a property first!");
        }
    } 

    /**
     * Switch the scene to edit properties
     * It will pass a selected Property Object to be pre loaded in the form
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToEditProperty(ActionEvent event) throws IOException {
        FXMLLoader loader = super.loadFXML("editproperty");
        Scene editPropertyScene = new Scene(loader.load());
        
        try {
            //Gets the controller of the new EditProperties View
            EditPropertyController controller = loader.getController();
            //Initializes the data of the new view with the selected property object
            controller.initializeProperty(tableView.getSelectionModel().getSelectedItem());
            super.showScene(event,editPropertyScene);
        } catch (NullPointerException e) {
            super.displayError("You must select a property first!");
        }
    }
    
    /**
     * Switch the scene to detailed properties
     * It will pass in a Property object that the user selected in the table to 
     * display a detailed view of the Property object
     * @param event 
     * @throws IOException 
     */
    @FXML
    private void switchToDetailedProperty(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("editproperty");
        Scene detailedPropertyScene = new Scene(loader.load());
        
        try {
            //Gets the controller of the new DetatiledProperty View
            EditPropertyController controller = loader.getController();
            //Initializes the data of the new view with the selected property object
            controller.initializeProperty(tableView.getSelectionModel().getSelectedItem());
            //Sets the scene to read only (User just wants to view the property)
            controller.setReadOnly();
            showScene(event,detailedPropertyScene);
        } catch (NullPointerException  e){
            super.displayError("You must select a property first!");
        }
    }
    
    /**
     * Initializes the TableView with data
     */
    @FXML
    private void initialize() {
        /*Setup columns*/
        propertyNameCol.setCellValueFactory(new PropertyValueFactory<Property,String>("propertyName"));
        propertyTypeCol.setCellValueFactory(new PropertyValueFactory<Property,String>("propertyType"));
        propertyAddressCol.setCellValueFactory(new PropertyValueFactory<Property,String>("propertyAddress"));
        propertyCostCol.setCellValueFactory(new PropertyValueFactory<Property,Integer>("propertyCost"));
        propertyDoorsCol.setCellValueFactory(new PropertyValueFactory<Property,Integer>("propertyDoors"));
        
        //Load test data
        tableView.setItems(getProperties());

    }
    
    /**
     * Gets all properties
     * @return ObservervableList of Properties
     */
    private ObservableList<Property> getProperties() {
        //Test Data (Load all properties from the database)
        ObservableList<Property> properties = FXCollections.observableArrayList();
        PropertyManager pm = new PropertyManager();
        properties = pm.loadAllProperties();
        return properties;
    }
    
}
