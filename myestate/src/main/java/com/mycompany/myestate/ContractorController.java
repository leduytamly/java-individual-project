/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Contractor;
import com.mycompany.myestate.models.ContractorManager;
import java.io.IOException;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *Controller for Contractor View
 * @author leduy
 */
public class ContractorController extends AController{
    
    /*FXML ids for the table and columns*/
    @FXML private TableView<Contractor> tableView;
    @FXML private TableColumn<Contractor,String> contractorNameCol;
    @FXML private TableColumn<Contractor,String> contractorCompanyCol;
    @FXML private TableColumn<Contractor,String> contractorPhoneNumberCol;
    @FXML private TableColumn<Contractor,String> contractorEmailCol;
    
    /**
     * Switch the scene to New Contractor
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToNewContractor(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("newcontractor");
        Scene newContractorScene = new Scene(loader.load());
        showScene(event,newContractorScene);
    }
    
    /**
     * Deletes a contractor from the database
     * @throws IOException 
     */
    @FXML
    private void deleteContractor() throws IOException {
        ContractorManager cm = new ContractorManager();
        try{
            Contractor contractor = tableView.getSelectionModel().getSelectedItem();
            if (contractor == null){
                throw new NullPointerException("no property selected");
            }
            if (switchConfirmationBox("Are you sure you want to delete? This cannot be undone!")){
                cm.deleteContractor(contractor);
                initialize();
            } 
        } catch (NullPointerException e){
            displayError("You must select a contractor first!");
        } 
    } 
    
    /**
     * Switch the scene to Edit Contractor
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToEditContractor(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("editcontractor");
        Scene editContractorScene = new Scene(loader.load());
        
        try {
            //Gets the controller of the new EditContractor View
            EditContractorController controller = loader.getController();
            //Initializes the data of the new view with the selected contractor object
            controller.initializeContractor(tableView.getSelectionModel().getSelectedItem());
            showScene(event,editContractorScene);
        } catch (NullPointerException  e){
            displayError("You must select a contractor first!");
        }
    }
    
    /**
     * Initializes the tables  
     */
    @FXML
    private void initialize() {
        /*Setup columns*/
        contractorNameCol.setCellValueFactory(new PropertyValueFactory<Contractor,String>("contractorName"));
        contractorCompanyCol.setCellValueFactory(new PropertyValueFactory<Contractor,String>("contractorCompany"));
        contractorPhoneNumberCol.setCellValueFactory(new PropertyValueFactory<Contractor,String>("contractorPhoneNumber"));
        contractorEmailCol.setCellValueFactory(new PropertyValueFactory<Contractor,String>("contractorEmail"));
        
        //Load data from the database
        tableView.setItems(getContractors());
    }
    
    /**
     * Gets all contractors from the database
     * @return ObservableList of all contractors
     */
    private ObservableList<Contractor> getContractors(){
        ContractorManager cm = new ContractorManager();
        ObservableList<Contractor> contractors = cm.loadAllContractors();
        return contractors;
    }
}
