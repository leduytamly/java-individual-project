/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import java.util.HashSet;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * Abstract class for Edit and New Contractor Controllers
 * @author leduy
 */
public class AContractorController extends AController {
        
    /*FXML ids for the inputs*/
    @FXML protected TextField contractorName;
    @FXML protected TextField contractorCompanyName;
    @FXML protected TextField contractorPhoneNumber;
    @FXML protected TextField contractorEmail;
    @FXML protected TextField contractorProfession;
    
    /**
     * Sets all listeners for input fields for Contractor
     */
    protected void setAllListeners(){
        setListenerEmail();
        setListenerPhoneNumber();
    }
    
    /**
     * Validates all fields for Contractor
     */
    protected void validateAllFields(){
        validateEmptyTextField();
        
        super.isValid();
    }
    
     /**
     * Validates for empty date fields
     */
    private void validateEmptyTextField(){
        HashSet<TextField> textFields = new HashSet<>();
        textFields.add(contractorName);
        textFields.add(contractorCompanyName);
        textFields.add(contractorPhoneNumber);
        textFields.add(contractorEmail);
        textFields.add(contractorProfession);
        
        super.validateEmptyTextField(textFields);
    }
    
    /**
     * Sets listeners for email field(s)
     */
    private void setListenerEmail(){
        HashSet<TextField> tf = new HashSet<>();
        tf.add(contractorEmail);
        
        super.validateEmail(tf);
    }
    
    /**
     * Sets listener for phone number field(s)
     */
    private void setListenerPhoneNumber(){
        HashSet<TextField> tf = new HashSet<>();
        tf.add(contractorPhoneNumber);
        
        super.validatePhoneNumber(tf);
    }
}
