/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Mortgage;
import com.mycompany.myestate.models.Property;
import java.io.IOException;
import java.util.HashSet;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;

/**
 * Abstract controller for banks
 * @author leduy
 */
public class ABankController extends AController{
    /*Input fields*/
    @FXML protected TextField bankName;
    @FXML protected TextField interestRate;
    
    /*Property and Mortgage object(to save the progress of the form)*/
    protected Mortgage mortgage;
    protected Property property;
    
    /**
     * Sets all the listeners
     */
    protected void setAllListeners(){
        setListeneresNumberField();
    }
    
    /**
     * validates all fields
     */
    protected void validateAllFields(){
        validateEmptyTextField();
        
        super.isValid();
    }
    
    /**
     * Switch the scene to the main bank view
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void switchToBanks(ActionEvent event) throws IOException {
        FXMLLoader loader;
        Scene newBankScene;
        
        /*If banks is open from renew mortgage it will return to banks with mortgage initialized*/
        if (mortgage != null) {
            loader = super.loadFXML("banks");
            newBankScene = new Scene(loader.load());
            BankController controller = loader.getController();
            controller.initializeMortgage(mortgage);
        } 
        /*If banks is open from new mortgage it will retrun to bank with propertyu initialized*/
        else {
            loader = super.loadFXML("banks");
            newBankScene = new Scene(loader.load());
            BankController controller = loader.getController();
            controller.initializeProperty(property);
        }
        super.showScene(event,newBankScene);
    }
    
    /**
     * Initialize the mortgage if banks is open from Renew Mortgage
     * @param mortgage Mortgage to initialize
     */
    public void initializeMortgage(Mortgage mortgage){
        this.mortgage = mortgage;
    }
    
    /**
     * Initialize the property if banks is open from New Mortgage
     * @param property Property to initialize
     */
    public void initializeProperty(Property property){
        this.property = property;
    }
    
    /**
     * Sets event listeners for numerical fields
     */
    private void setListeneresNumberField() {
        HashSet<TextField> numberFields = new HashSet<>();
        numberFields.add(interestRate);
        super.validateNumbers(numberFields);
    }
    
    /**
     * Throws an exception if a non-numerical TextField is empty
     */
    private void validateEmptyTextField() {
        HashSet<TextField> textFields = new HashSet<>();
        textFields.add(bankName);
        textFields.add(interestRate);
        
        super.validateEmptyTextField(textFields);
    }
}
