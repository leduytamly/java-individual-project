/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.exceptions.PropertyIsFullException;
import com.mycompany.myestate.models.Lease;
import com.mycompany.myestate.models.Property;
import com.mycompany.myestate.models.Tenant;
import com.mycompany.myestate.models.TenantManager;
import java.io.IOException;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 *Controller Class For New Tenant View
 * @author leduy
 */
public class NewTenantController extends ATenantController{
    
    /**
     * Initializes the view
     * @throws SQLException 
     */
    @FXML
    private void initialize() throws SQLException{
        super.setup();
        super.setAllListeners();
    }
    
    /**
     * Creates a lease and makes a backup of the pdf
     * @return Lease object
     */
    private Lease createLease(){
        Lease lease = new Lease(leaseStartDate.getValue(), leaseEndDate.getValue(),path.getText());
        String filename = ((Property)property.getValue()).getPropertyName() + "_" + tenantFullname.getText();
        lease.saveFile(filename);
        return lease;
    }
    
    /**
     * Creates a tenant object
     * @return Tenant Object
     */
    private Tenant createTenant(){
        Tenant tenant = new Tenant(tenantFullname.getText(),(Property)property.getValue(),
                tenantPhoneNumber.getText(),
                tenantEmail.getText(),
                rentStartDate.getValue(), 
                rentEndDate.getValue(),
                Double.parseDouble(tenantYearlyIncome.getText()),
                Double.parseDouble(tenantIncomeDebtRatio.getText()), 
                super.translateStatusChoiceBox(),
                (String)tenantPaymentMethod.getValue(),
                createLease());
        return tenant;
    }
    
    /**
     * Inserts the tenant and lease object in the database 
     * @throws SQLException 
     */
    @FXML
    private void makeInsert(ActionEvent event) throws IOException{
        try {
            super.validateAllFields();
            
            Tenant tenant = createTenant();
            TenantManager tm = new TenantManager();
            super.checkPropertyIsFull(tenant);
            
            int tenantPk = tm.insertTenant(tenant);
            tm.insertLease(tenant.getLease(), tenantPk);
            
            super.switchToTenants(event);
        } catch (PropertyIsFullException e){
            System.out.println(e);
            switchWarningBox("Property is full! Please change property");
        } catch (IllegalArgumentException e){
            System.out.println(e);
            switchWarningBox("Missing values!");
        }
    }
    
}
