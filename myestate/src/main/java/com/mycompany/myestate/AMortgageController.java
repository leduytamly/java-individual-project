/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import java.util.HashSet;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Abstract class for Edit and New Mortgage Controllers
 * @author leduy
 */
public class AMortgageController extends AController{
    
    /*FXML ids for inputs*/
    @FXML protected Label propertyName;
    @FXML protected ChoiceBox bank;
    @FXML protected DatePicker nextPaymentDate;
    @FXML protected DatePicker startDate;
    @FXML protected DatePicker endDate;
    @FXML protected TextField downPayment;
    
    /**
     * Sets all listeners 
     */
    protected void setAllListeners(){
        setListeneresNumberField();
    }
    
    /**
     * Validates all fields
     */
    protected void validateAllFields(){
        validateEmptyTextField();
        validateEmptyDateField();
        validateEmptyChoiceBox();
        
        super.isValid();
    }
    
    /**
     * Sets event listeners for numerical fields
     */
    private void setListeneresNumberField() {
        HashSet<TextField> numberFields = new HashSet<>();
        numberFields.add(downPayment);
        super.validateNumbers(numberFields);
    }
    
    /**
     * Throws an exception if a non-numerical TextField is empty 
     */
    private void validateEmptyTextField() {
        HashSet<TextField> textFields = new HashSet<>();
        textFields.add(downPayment);
        
        super.validateEmptyTextField(textFields);
    }
    
    /**
     * Validates for empty date fields
     */
    private void validateEmptyDateField(){
        HashSet<DatePicker> dateFields = new HashSet<>();
        dateFields.add(nextPaymentDate);
        dateFields.add(startDate);
        dateFields.add(endDate);
        
        super.validateEmptyDateField(dateFields);
    }
    
    /**
     * Validate for empty bank field
     */
    private void validateEmptyChoiceBox(){
        HashSet<ChoiceBox> choiceBoxes = new HashSet<>();
        choiceBoxes.add(bank);
        
        super.validateEmptyChoiceBox(choiceBoxes);
    }
}
