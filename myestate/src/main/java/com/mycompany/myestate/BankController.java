/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.BankInstitution;
import com.mycompany.myestate.models.BankInstitutionManager;
import com.mycompany.myestate.models.Contractor;
import java.io.IOException;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Controller for main bank view
 * @author leduy
 */
public class BankController extends ABankController{
    /*FXML ids for the table and columns*/
    @FXML private TableView<BankInstitution> tableView;
    @FXML private TableColumn<Contractor,String> bankNameCol;
    @FXML private TableColumn<Contractor,Double> interestCol;
    
    /**
     * Initializes the tables  
     */
    @FXML
    private void initialize() {
        /*Setup columns*/
        bankNameCol.setCellValueFactory(new PropertyValueFactory<Contractor,String>("institutionName"));
        interestCol.setCellValueFactory(new PropertyValueFactory<Contractor,Double>("interestRate"));
        
        //Load data from the database
        tableView.setItems(getBanks());
    }
    
    /**
     * Switch the scene to the edit or new  mortgages view
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void switchToMortgage(ActionEvent event) throws IOException {
        FXMLLoader loader;
        Scene mortgageScene;
        
        /*If banks is open from renew mortgage it will return to renew mortgage*/
        if (super.mortgage != null) {
            loader = super.loadFXML("renewmortgage");
            mortgageScene = new Scene(loader.load());
            RenewMortgageController controller = loader.getController();
            controller.initializeMortgage(super.mortgage);
        } 
        /*If banks is open from new mortgage it will retrun to new mortgage*/
        else {
            loader = super.loadFXML("newmortgage");
            mortgageScene = new Scene(loader.load());
            NewMortgageController controller = loader.getController();
            controller.initializeProperty(super.property);
        }
        super.showScene(event,mortgageScene);
    }
    
    /**
     * Switch the scene to New Bank
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToNewBank(ActionEvent event) throws IOException {
        FXMLLoader loader;
        Scene newBankScene;
        
        /*If banks is open from renew mortgage it will return to bank wiht mortgage initialize*/
        if (super.mortgage != null) {
            loader = super.loadFXML("newbank");
            newBankScene = new Scene(loader.load());
            NewBankController controller = loader.getController();
            controller.initializeMortgage(super.mortgage);
        } 
        /*If banks is open from new mortgage it will retrun to bank with property initialize*/
        else {
            loader = super.loadFXML("newbank");
            newBankScene = new Scene(loader.load());
            NewBankController controller = loader.getController();
            controller.initializeProperty(super.property);
        }
        super.showScene(event,newBankScene);
    }
    
    /**
     * Switch the scene to Edit Bank
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToEditBank(ActionEvent event) throws IOException {
        FXMLLoader loader;
        Scene editBankScene;
        
        /*If banks is open from renew mortgage it will return to bank wiht mortgage initialize*/
        try{
            if (super.mortgage != null) {
                loader = super.loadFXML("editbank");
                editBankScene = new Scene(loader.load());
                EditBankController controller = loader.getController();
                controller.initializeMortgage(super.mortgage);
                controller.initializeBank(tableView.getSelectionModel().getSelectedItem());
            } 
            /*If banks is open from new mortgage it will retrun to bank with property initialize*/
            else {
                loader = super.loadFXML("editbank");
                editBankScene = new Scene(loader.load());
                EditBankController controller = loader.getController();
                controller.initializeProperty(super.property);
                controller.initializeBank(tableView.getSelectionModel().getSelectedItem());
            }
            super.showScene(event,editBankScene);
        } catch (NullPointerException  e){
            displayError("You must select a Bank first!");
        }
    }
    
    /**
     * Deletes a Bank, Displays a confirmation box before
     * @throws IOException 
     */
    @FXML
    private void deleteBank() throws IOException {
        BankInstitutionManager bim = new BankInstitutionManager();
        try{
            BankInstitution bank = tableView.getSelectionModel().getSelectedItem();
            if (bank == null){
                throw new NullPointerException("no bank selected");
            }
            if (switchConfirmationBox("Are you sure you want to delete? This cannot be undone!")){
                bim.deleteBank(bank);
                initialize();
            } 
        } catch (NullPointerException e){
            super.displayError("You must select a bank first!");
        }
    } 
    
    /**
     * Gets all contractors from the database
     * @return ObservableList of all contractors
     */
    private ObservableList<BankInstitution> getBanks(){
        BankInstitutionManager bim = new BankInstitutionManager();
        ObservableList<BankInstitution> banks = bim.loadAllBanks();
        return banks;
    }
}
