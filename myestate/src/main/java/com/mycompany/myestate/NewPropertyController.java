/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Condo;
import com.mycompany.myestate.models.House;
import com.mycompany.myestate.models.Insurance;
import com.mycompany.myestate.models.Plex;
import com.mycompany.myestate.models.Property;
import java.io.IOException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

/**
 *Controller Class for New Property View
 * @author leduy
 */
public class NewPropertyController extends APropertyController{
    
    /*Property Object (Used if a user comes back from adding a new mortgage)*/
    private Property createdProperty;
    
    /**
     * Initializes the view and ChoiceBoc, hides condo fees by default
     */
    @FXML
    private void initialize() {    
        super.setAllListeners();
        
        /*Initialize choice box and default value*/
        super.initializeChoiceBox();
        propertyType.setValue("Plex");
        
        setChoiceBoxListenner();
        
        super.displayCondoFee(false);
    }
    
    /**
     * Switch the scene to New Mortgage
     * (When adding a property the user must add a mortgage to it)
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToAddMortgage(ActionEvent event) throws IOException {
        FXMLLoader loader = super.loadFXML("newmortgage");
        Scene newMortgageScene = new Scene(loader.load());
        
        //Gets the controller of the new newmortgage View
        NewMortgageController controller = loader.getController();

       try {
            /*Data validation*/ 
            super.validateAllFields();
            /*Creates the property and sends it to the new mortgage view*/
            controller.initializeProperty(createProperty());
            super.showScene(event,newMortgageScene);
        } catch (IllegalArgumentException e) {
            System.out.println(e);
            switchWarningBox("Missing values!");
        }
    }
    
    /**
     * Initializes the inputs to create a new property(Only used when the user
     * comes back from adding a new mortgage)
     * @param property Created Property
     */
    public void initializeProperty (Property property){
        /*Values common to all properties*/
        createdProperty = property;
        propertyName.setText(createdProperty.getPropertyName());
        propertyType.setValue(createdProperty.getPropertyType());
        propertyAddress.setText(createdProperty.getPropertyAddress());
        propertyCost.setText(String.valueOf(createdProperty.getPropertyCost()));
        propertyDoors.setText(Integer.toString(createdProperty.getPropertyDoors()));
        propertyRentalPrice.setText(String.valueOf(createdProperty.getPropertyRentalPrice()));
        propertyTaxRate.setText(String.valueOf(createdProperty.getPropertyTaxRate()));
        propertySchoolTaxRate.setText(String.valueOf(createdProperty.getPropertySchoolTaxRate()));
        insuranceCompany.setText(createdProperty.getInsurance().getInsuranceName());
        intrestRate.setText(String.valueOf(createdProperty.getInsurance().getInsuranceAmount()));
        
        /*Values specific to a type of property*/
        super.initializSpecificProperty(createdProperty);
    }
    
    /**
     * Initialize ChoiceBox values and behavior when user change value
     */
    private void setChoiceBoxListenner(){
        propertyType.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String oldV, String newV) {
                switch (newV) {
                    case "Condo":
                        propertyDoors.setText("1");
                        propertyDoors.setDisable(true);
                        displayCondoFee(true);
                        break;
                    case "House":
                        propertyDoors.setText("1");
                        propertyDoors.setDisable(true);
                        displayCondoFee(false);
                        break;
                    default:
                        propertyDoors.setText("");
                        propertyDoors.setDisable(false);
                        displayCondoFee(false);
                        break;
                }
            }
        });
    }
    
    /**
     * Creates a Property Object depending on what type it is using the inputs 
     * @return Property Object
     */
    private Property createProperty(){
        Property p;
        if (propertyType.getValue().equals("Condo")){
            p = new Condo(
                propertyName.getText(),
                propertyAddress.getText(),
                Double.parseDouble(propertyCost.getText()),
                Double.parseDouble(propertyRentalPrice.getText()),
                Double.parseDouble(propertyTaxRate.getText()),
                Double.parseDouble(propertySchoolTaxRate.getText()),
                new Insurance(insuranceCompany.getText(),
                Double.parseDouble(intrestRate.getText())),
                Double.parseDouble(condoFee.getText()));
        }
        else if (propertyType.getValue().equals("House")){
            p = new House(
                propertyName.getText(),
                propertyAddress.getText(),
                Double.parseDouble(propertyCost.getText()),
                Double.parseDouble(propertyRentalPrice.getText()),
                Double.parseDouble(propertyTaxRate.getText()),
                Double.parseDouble(propertySchoolTaxRate.getText()),
                new Insurance(insuranceCompany.getText(),
                Double.parseDouble(intrestRate.getText())));
        }
        else {
            p = new Plex(
                propertyName.getText(),
                propertyAddress.getText(),
                Double.parseDouble(propertyCost.getText()),
                Integer.parseInt(propertyDoors.getText()),
                Double.parseDouble(propertyRentalPrice.getText()),
                Double.parseDouble(propertyTaxRate.getText()),
                Double.parseDouble(propertySchoolTaxRate.getText()),
                new Insurance(insuranceCompany.getText(),
                Double.parseDouble(intrestRate.getText())));
        }
        return p;
    }
}
