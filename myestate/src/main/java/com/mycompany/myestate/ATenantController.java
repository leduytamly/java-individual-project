/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.exceptions.PropertyIsFullException;
import com.mycompany.myestate.models.PropertyManager;
import com.mycompany.myestate.models.Tenant;
import com.mycompany.myestate.models.TenantManager;
import java.io.File;
import java.sql.SQLException;
import java.util.HashSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * Abstract class for edit tenant and new tenant
 * @author leduy
 */
public abstract class ATenantController extends AController{
    
    /*FXML ids for input fields*/
    @FXML protected TextField tenantFullname;
    @FXML protected ChoiceBox property;
    @FXML protected TextField tenantPhoneNumber;
    @FXML protected TextField tenantEmail;
    @FXML protected DatePicker rentStartDate;
    @FXML protected DatePicker rentEndDate;
    @FXML protected TextField tenantYearlyIncome;
    @FXML protected TextField tenantIncomeDebtRatio;
    @FXML protected ChoiceBox tenantStatus;
    @FXML protected ChoiceBox tenantPaymentMethod;
    
    /*For lease lease*/
    @FXML protected DatePicker leaseStartDate;
    @FXML protected DatePicker leaseEndDate;
    @FXML protected Label path;
    
    /**
     * Sets up the choice boxes
     * @throws SQLException 
     */
    protected void setup() throws SQLException{
        initializePropertyChoiceBox();
        initializePaymentMethodChoiceBox();
        initializeStatusChoiceBox();
    }
    
    /**
     * Sets all listeners
     */
    protected void setAllListeners(){
        setListeneresNumberField();
        setListenersEmail();
        setListenersPhoneNumber();
    }
    
    /**
     * Validates all the fields
     */
    protected void validateAllFields(){
        validateEmptyTextField();
        validateEmptyDateField();
        validateEmptyChoiceBox();
        
        super.isValid();
    }
    
    /**
     * Initializes the choice box for properties
     * @throws SQLException 
     */
    private void initializePropertyChoiceBox() {
        PropertyManager pm = new PropertyManager();
        property.setItems(pm.loadAllProperties());
    }
    
    /**
     * Initializes the choice box for Payment method
     */
    private void initializePaymentMethodChoiceBox(){
        ObservableList<String> options = FXCollections.observableArrayList("Direct Deposit","Cheque","Cash");
        tenantPaymentMethod.setItems(options);
    }
    
    /**
     * Initializes the choice box for Status
     */
    private void initializeStatusChoiceBox(){
        ObservableList<String> options = FXCollections.observableArrayList("Paid","Unpaid");
        tenantStatus.setItems(options);
    }
    
    /**
     * Translates the choice box input into Boolean
     * @return Paid is true, Unpaid is false
     */
    protected boolean translateStatusChoiceBox(){
        return ((String)tenantStatus.getValue()).equals("Paid");
    }
    
    /**
     * Translate a Boolean into a choice box input
     * @param b true is paid, false is unpaid
     * @return String for the choice box
     */
    protected String translateStatusChoiceBox(Boolean b){
        System.out.println(b);
        if (b){
            return "Paid";
        }
        return "Unpaid";
    }
    
    /**
     * User can chose a pdf file for a lease
     */
    @FXML
    protected void choseLease(){
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new ExtensionFilter("PDF files","*.pdf"));
        File selectedFile = fc.showOpenDialog(null);
        
        if (selectedFile != null){
            path.setText(selectedFile.getAbsolutePath());
        } else {
            System.out.println("file does not exist");
        }
    }
    
    /**
     * Checks if a property is full capacity
     * @param tenant Tenant object
     * @throws PropertyIsFullException 
     */
    protected void checkPropertyIsFull(Tenant tenant) throws PropertyIsFullException{
        TenantManager tm = new TenantManager();
        boolean isPropertyFull = tm.getNumberOfTenantForAProperty(tenant.getProperty()) >= tenant.getProperty().getPropertyDoors();
        /*HashSet of all the tenants in that property*/
        HashSet<String> tenantList = tm.getTenantNamesForAProperty(tenant.getProperty());
        boolean isCurrentTenant = tenantList.contains(tenant.getTenantFullName());
        
        if (isPropertyFull && !isCurrentTenant){
            throw new PropertyIsFullException("Property is full");
        }
    }
    
    /**
     * Adds all the numerical input fields into a HashSet to attach event
     * listeners
     */
    private void setListeneresNumberField(){
        HashSet<TextField> numberFields = new HashSet<>();
        numberFields.add(tenantYearlyIncome);
        numberFields.add(tenantIncomeDebtRatio);
        
        /*Checks if the number is valid and instant live feedback*/
        super.validateNumbers(numberFields);
    }
    
    /**
     * Checks email field(s) if they are valid 
     */
    private void setListenersEmail(){
        HashSet<TextField> emailFields = new HashSet<>();
        emailFields.add(tenantEmail);
        super.validateEmail(emailFields);
    }
    
    /**
     * Checks phone number fields if they are valid
     */
    private void setListenersPhoneNumber(){
        HashSet<TextField> phoneNumberFields = new HashSet<>();
        phoneNumberFields.add(tenantPhoneNumber);
        super.validatePhoneNumber(phoneNumberFields);
    }
    
    /**
     * Throws an exception if a non-numerical TextField is empty 
     */
    private void validateEmptyTextField(){
        HashSet<TextField> textFields = new HashSet<>();
        textFields.add(tenantFullname);
        textFields.add(tenantPhoneNumber);
        textFields.add(tenantEmail);
        textFields.add(tenantIncomeDebtRatio);
        textFields.add(tenantYearlyIncome);
        
        super.validateEmptyTextField(textFields);
    }
    
    /**
     * Validates for empty date fields
     */
    private void validateEmptyDateField(){
        HashSet<DatePicker> dateFields = new HashSet<>();
        dateFields.add(rentStartDate);
        dateFields.add(rentEndDate);
        dateFields.add(leaseStartDate);
        dateFields.add(leaseEndDate);
        
        super.validateEmptyDateField(dateFields);
    }
    
    /**
     * Validates empty choice box fields
     */
    private void validateEmptyChoiceBox(){
        HashSet<ChoiceBox> choiceBoxes = new HashSet<>();
        choiceBoxes.add(property);
        choiceBoxes.add(tenantStatus);
        choiceBoxes.add(tenantPaymentMethod);
        super.validateEmptyChoiceBox(choiceBoxes);
    }
}
