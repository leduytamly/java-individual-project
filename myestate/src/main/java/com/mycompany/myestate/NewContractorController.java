/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Contractor;
import com.mycompany.myestate.models.ContractorManager;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 * Controller for New Contractor View
 * @author leduy
 */
public class NewContractorController extends AContractorController{
    
    /**
     * Initializes the new contractor view
     */
    @FXML
    private void initialize(){
        super.setAllListeners();
    }
    
    /**
     * Creates a contractor object with the input
     * @return Contractor object
     */
    private Contractor createContractor(){
        Contractor newContractor = new Contractor(
                contractorName.getText(),
                contractorCompanyName.getText(),
                contractorPhoneNumber.getText(),
                contractorEmail.getText(),
                contractorProfession.getText()
        );
        return newContractor;
    }
    
    /**
     * Inserts contractor into the database
     * @param event
     * @throws IOException 
     */
    @FXML
    private void makeInserts(ActionEvent event) throws IOException{
        try{
            /*Data validation*/
            super.validateAllFields();
            
            /*Creates the contractor and inserts it into the database*/
            ContractorManager cm = new ContractorManager();
            cm.insertContractor(createContractor());
            
            /*Switches the view back to Contractors*/
            switchToContractors(event);
        } catch (IOException e) {
            System.out.println(e);
            System.out.println("ERROR SHOWING WARNING");
        } catch(IllegalArgumentException e) {
            System.out.println(e);
            switchWarningBox("Missing values!");
        }
    }
}
