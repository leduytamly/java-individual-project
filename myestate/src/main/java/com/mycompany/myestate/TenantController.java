/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Tenant;
import com.mycompany.myestate.models.TenantManager;
import java.io.File;
import java.io.IOException;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.awt.Desktop;

/**
 *Controller Class For Tenant View
 * @author leduy
 */
public class TenantController extends AController {
    
    /*FXML ids for the table and columns*/
    @FXML private TableView<Tenant> tableView;
    @FXML private TableColumn<Tenant,String> tenantFullNameCol;
    @FXML private TableColumn<Tenant,String> propertyNameCol;
    @FXML private TableColumn<Tenant,String> tenantPhoneNumberCol;
    @FXML private TableColumn<Tenant,String> tenantEmailCol;
    
    /**
     * Switch the view to New Tenant
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToNewTenant(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("newtenant");
        Scene newTenantScene = new Scene(loader.load());
        showScene(event,newTenantScene);
    }
    
    /**
     * Deletes a tenant, Displays a confirmation box before
     * @throws IOException 
     */
    @FXML
    private void deleteTenant() throws IOException {
        TenantManager tm = new TenantManager();
        try{
            Tenant tenant = tableView.getSelectionModel().getSelectedItem();
            if (tenant == null){
                throw new NullPointerException("no tenant selected");
            }
            if (switchConfirmationBox("Are you sure you want to delete? This cannot be undone!")){
                tm.deleteTenant(tenant);
                initialize();
            } 
        } catch (NullPointerException e){
            super.displayError("You must select a tenant first!");
        }
    } 
    
    /**
     * Switch the view to Edit Tenant
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToEditTenant(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("edittenant");
        Scene editTenantScene = new Scene(loader.load());
        
        try {
            //Gets the controller of the new EditTanant View
            EditTenantController controller = loader.getController();
            //Initializes the data of the new view with the selected tenant object
            controller.initializeTenant(tableView.getSelectionModel().getSelectedItem());

            showScene(event,editTenantScene);
        } catch (NullPointerException e){
            super.displayError("You must select a tenant first!");
        }
    }
    
    /**
     * Switch the view to Edit Tenant (Read-Only)
     * @param event
     * @throws IOException 
     */
    @FXML
    private void switchToDetailedTenant(ActionEvent event) throws IOException {
        FXMLLoader loader = loadFXML("edittenant");
        Scene editTenantScene = new Scene(loader.load());
        
        try {
            //Gets the controller of the new EditTanant View
            EditTenantController controller = loader.getController();
            //Initializes the data of the new view with the selected tenant object
            controller.initializeTenant(tableView.getSelectionModel().getSelectedItem());

            controller.setReadOnly();

            showScene(event,editTenantScene);
        } catch (NullPointerException e){
            super.displayError("You must select a tenant first!");
        }
    }
    
    /**
     * Opens the file to display the lease
     * @throws IOException 
     */
    @FXML
    private void viewLease() throws IOException{
        Tenant t = tableView.getSelectionModel().getSelectedItem();
        try {
            File file = new File(t.getLease().getDestinationPath());
            if (file.exists()){
                Desktop.getDesktop().open(file);
            } else {
                super.displayError("File does not exist!");
            }
        } catch (NullPointerException e) {
            super.displayError("You must select a tenant first!");
        }
        
    }
    
    /**
     * Initializes the table
     */
    @FXML
    private void initialize() {
        /*Setup columns*/
        tenantFullNameCol.setCellValueFactory(new PropertyValueFactory<Tenant,String>("tenantFullName"));
        propertyNameCol.setCellValueFactory(new PropertyValueFactory<Tenant,String>("property"));
        tenantPhoneNumberCol.setCellValueFactory(new PropertyValueFactory<Tenant,String>("tenantPhoneNumber"));
        tenantEmailCol.setCellValueFactory(new PropertyValueFactory<Tenant,String>("tenantEmail"));
        
        //Load test data
        tableView.setItems(getTenants());
    }
    
    /**
     * Gets all tenants
     * @return ObservableList of all the tenants
     */
    private ObservableList<Tenant> getTenants() {
        
        //Sample Date (Retrieve from database)
        TenantManager tm = new TenantManager();
        
        return tm.loadAllTenant();
    }
}
