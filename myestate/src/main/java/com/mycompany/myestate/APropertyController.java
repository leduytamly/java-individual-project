/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Condo;
import com.mycompany.myestate.models.House;
import com.mycompany.myestate.models.Property;
import java.util.HashSet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Abstract class for Edit and New Property Controllers
 * @author leduy
 */
public abstract class APropertyController extends AController{
    
    @FXML protected TextField propertyName;
    @FXML protected TextField propertyAddress;
    @FXML protected TextField propertyCost;
    @FXML protected TextField propertyDoors;
    @FXML protected TextField propertyRentalPrice;
    @FXML protected TextField propertyTaxRate;
    @FXML protected TextField propertySchoolTaxRate;
    @FXML protected TextField insuranceCompany;
    @FXML protected TextField intrestRate;
    @FXML protected TextField condoFee;
    @FXML protected Label condoFeeLabel;
    /*FXML ids for the inputs to add a new property*/
    @FXML protected ChoiceBox propertyType;
    
    /**
     * Set listeners for all fields
     */
    protected void setAllListeners(){
        setListeneresNumberField();
    }
    
    /**
     * Validates all fields
     */
    protected void validateAllFields(){
        validateEmptyTextField();
        super.isValid();
    }
    
    /**
     * Preloads or remove fields depending on the type of property
     * @param selectedProperty
     */
    protected void initializSpecificProperty(Property selectedProperty){
        if (selectedProperty instanceof Condo){
            initializeCondo(selectedProperty);
        } 
        else if (selectedProperty instanceof House){
            initializeHouse();
        }
        else {
            initializePlex();
        }
    }
    
    /**
     * Displays the condo fee label and TextField
     * @param b Displays or hides condo fees
     */
    protected void displayCondoFee(boolean b){
        condoFeeLabel.setVisible(b);
        condoFee.setVisible(b);
    }
    
    /**
     * Initializes choice box with property type
     */
    protected void initializeChoiceBox(){
        ObservableList<String> types = FXCollections.observableArrayList("House","Plex","Condo");
        propertyType.setItems(types);
    }
    
    /**
     * Accommodate view a Condo
     */
    private void initializeCondo(Property selectedProperty){
        condoFee.setText(String.valueOf(((Condo)selectedProperty).getCondoFee()));
        propertyDoors.setDisable(true);
        displayCondoFee(true);
    }
    
    /**
     * Accommodate view for a House
     */
    private void initializeHouse(){
        propertyDoors.setDisable(true);
        displayCondoFee(false);
    }
    
    /**
     * Accommodate view for a Plex
     */
    private void initializePlex(){
        displayCondoFee(false);
    }
    
    /**
     * Throws an exception if a non-numerical TextField is empty 
     */
    private void validateEmptyTextField(){
        HashSet<TextField> textFields = new HashSet<>();
        textFields.add(propertyName);
        textFields.add(propertyAddress);
        textFields.add(insuranceCompany);
        textFields.add(propertyCost);
        textFields.add(propertyDoors);
        textFields.add(propertyRentalPrice);
        textFields.add(propertyTaxRate);
        textFields.add(propertySchoolTaxRate);
        textFields.add(intrestRate);
        
        /*If the type is a condo, check for condo field*/
        if (propertyType.getValue() == "Condo")
            textFields.add(condoFee);
        
        super.validateEmptyTextField(textFields);
    }
    
    /**
     * Adds all the numerical input fields into a HashSet to attach event
     * listeners
     */
    private void setListeneresNumberField(){
        HashSet<TextField> numberFields = new HashSet<>();
        numberFields.add(propertyCost);
        numberFields.add(propertyDoors);
        numberFields.add(propertyRentalPrice);
        numberFields.add(propertyTaxRate);
        numberFields.add(propertySchoolTaxRate);
        numberFields.add(intrestRate);
        numberFields.add(condoFee);
        
        /*Checks if the number is valid and instant live feedback*/
        super.validateNumbers(numberFields);
    }
}
