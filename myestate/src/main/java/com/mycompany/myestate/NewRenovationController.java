/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.models.Contractor;
import com.mycompany.myestate.models.Property;
import com.mycompany.myestate.models.Renovation;
import com.mycompany.myestate.models.RenovationManager;
import java.io.IOException;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 *Controller Class For New Renovation View
 * @author leduy
 */
public class NewRenovationController extends ARenovationController{
    /**
     * Initializes all choice boxes and number field validation
     * @throws SQLException 
     */
    @FXML
    private void initialize() throws SQLException{
        super.setup();
        super.setAllListeners();
    }
    
    /**
     * Creates a new renovation with the input fields
     * @return Renovation object
     */
    private Renovation createRenovation(){
        Renovation renovation = new Renovation(
                (Contractor)contractor.getValue(),
                (Property)property.getValue(),
                renovationType.getText(),
                renovationDescription.getText(),
                Double.parseDouble(renovationCost.getText()),
                super.translateStatusChoiceBox()
                
        );
        return renovation;
    }
    
    /**
     * Inserts Renovation into the database
     * @param event
     * @throws IOException 
     */
    @FXML
    private void makeInserts(ActionEvent event) throws IOException{
        try{
            /*Data Validation before creating new renovation*/
            super.validateAllFields();
            
            RenovationManager rm = new RenovationManager();
            rm.insertRenovation(createRenovation());
            switchToRenovations(event);
        } catch (IOException e) {
            System.out.println(e);
            System.out.println("ERROR SHOWING WARNING");
        } catch(IllegalArgumentException e) {
            System.out.println(e);
            switchWarningBox("Missing values!");
        }
    }
}
