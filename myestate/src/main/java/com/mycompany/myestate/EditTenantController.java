/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myestate;

import com.mycompany.myestate.exceptions.PropertyIsFullException;
import com.mycompany.myestate.models.Lease;
import com.mycompany.myestate.models.Property;
import com.mycompany.myestate.models.Tenant;
import com.mycompany.myestate.models.TenantManager;
import java.io.IOException;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *Controller Class for Edit Tenant Controller
 * @author leduy
 */
public class EditTenantController extends ATenantController{
    
    /*Tenant Object*/
    private Tenant selectedTenant;
    
    /*FXML ids for the header*/
    @FXML private Label header;
    /*Confirm Button*/
    @FXML private Button confirmBtn;
    
    /**
     * Initializes the view
     * @throws SQLException 
     */
    @FXML
    private void initialize() throws SQLException{
        super.setup();
        super.setAllListeners();
    }
    
    /**
     * Initializes the tenant with the fields pre loaded
     * @param tenant Tenant Object to pre load
     */
    public void initializeTenant(Tenant tenant){
        selectedTenant = tenant;
        
        /*For tenant object*/
        tenantFullname.setText(selectedTenant.getTenantFullName());
        property.setValue(selectedTenant.getProperty());
        tenantPhoneNumber.setText(selectedTenant.getTenantPhoneNumber());
        tenantEmail.setText(selectedTenant.getTenantEmail());
        rentStartDate.setValue(selectedTenant.getTenantRentStart());
        rentEndDate.setValue(selectedTenant.getTenantRentEnd());
        tenantYearlyIncome.setText(Double.toString(selectedTenant.getTenantYearlyIncome()));
        tenantIncomeDebtRatio.setText(Double.toString(selectedTenant.getTenantIncomeDebtRatio()));
        tenantStatus.setValue(super.translateStatusChoiceBox(selectedTenant.getTenantStatus()));
        tenantPaymentMethod.setValue(selectedTenant.getTenantPaymentMethond());
        
        /*For lease object*/
        leaseStartDate.setValue(selectedTenant.getLease().getStartdate());
        leaseEndDate.setValue(selectedTenant.getLease().getEnddate());
        path.setText(selectedTenant.getLease().getDestinationPath());
        
        
    }
    
    /**
     * Disables all input fields for viewing only
     */
    public void setReadOnly(){
        /*Setting the header*/
        header.setText("Overview of a Tenant");
        
        /*Disable all the input fields*/
        tenantFullname.setDisable(true);
        property.setDisable(true);
        tenantPhoneNumber.setDisable(true);
        tenantEmail.setDisable(true);
        rentStartDate.setDisable(true);
        rentEndDate.setDisable(true);
        tenantYearlyIncome.setDisable(true);
        tenantIncomeDebtRatio.setDisable(true);
        tenantStatus.setDisable(true);
        tenantPaymentMethod.setDisable(true);
        
        /*For lease*/
        leaseStartDate.setDisable(true);
        leaseEndDate.setDisable(true);
        path.setDisable(true);
        confirmBtn.setVisible(false);
    }
    
    /**
     * Sets the new values for a tenant
     */
    private void setTenant(){
        selectedTenant.setTenantFullName(tenantFullname.getText());
        selectedTenant.setProperty((Property)property.getValue());
        selectedTenant.setTenantPhoneNumber(tenantPhoneNumber.getText());
        selectedTenant.setTenantEmail(tenantEmail.getText());
        selectedTenant.setTenantRentStart(rentStartDate.getValue());
        selectedTenant.setTenantRentEnd(rentEndDate.getValue());
        selectedTenant.setTenantYearlyIncome(Double.parseDouble(tenantYearlyIncome.getText()));
        selectedTenant.setTenantIncomeDebtRation(Double.parseDouble(tenantIncomeDebtRatio.getText()));
        selectedTenant.setTenantStatus(super.translateStatusChoiceBox());
        selectedTenant.setTenantPaymentMethond((String)tenantPaymentMethod.getValue());
    }
    
    /**
     * Sets new values for lease
     */
    private void setLease(){
        Lease lease = selectedTenant.getLease();
        lease.setStartdate(leaseStartDate.getValue());
        lease.setEnddate(leaseEndDate.getValue());
        lease.setSourcePath(path.getText());
        
        String filename = selectedTenant.getProperty().getPropertyName() + "_" + selectedTenant.getTenantFullName();
        
        lease.saveFile(filename);
        
        selectedTenant.setLease(lease);
    }
    
    /**
     * Updates the database with the tenant with new values
     * @throws IOException 
     */
    @FXML
    private void makeChange(ActionEvent event) throws IOException{
        if (switchConfirmationBox("Are you sure you want to make the change?")){
            try {
                /*Data validation*/
                super.validateAllFields();
                /*Sets the new values*/
                setTenant();
                setLease();
                /*Checks if a property is full*/
                super.checkPropertyIsFull(selectedTenant);
                
                /*Makes the update*/
                TenantManager tm = new TenantManager();
                tm.updateTenant(selectedTenant);
                tm.updateLease(selectedTenant.getLease());
                
                /*Changes the view back to the main tenant view*/
                super.switchToTenants(event);
                
            } catch (PropertyIsFullException e){
                System.out.println(e);
                switchWarningBox("Property is full! Please change property");
            } catch (IllegalArgumentException e){
                System.out.println(e);
                switchWarningBox("Missing values!");
            }
        }
    }
}
