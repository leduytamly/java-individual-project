DROP schema myestate_db;
CREATE DATABASE myestate_db;
USE myestate_db;

CREATE TABLE bank_institution(
	bank_id int PRIMARY KEY AUTO_INCREMENT,
    institution_name varchar(128), 
    interest_rate double
);

CREATE TABLE mortgage(
	mortgage_id int PRIMARY KEY AUTO_INCREMENT,
    bank_id int,
    payment_date date,
    start_date date,
    end_date date,
    down_payment double,
    CONSTRAINT fk_bank_id 
    FOREIGN KEY (bank_id) 
    REFERENCES bank_institution(bank_id) ON DELETE CASCADE
);

CREATE TABLE insurance(
	insurance_id int PRIMARY KEY AUTO_INCREMENT,
    insurance_name varchar(128),
    insurance_amount double
);

CREATE TABLE property (
	property_id int PRIMARY KEY AUTO_INCREMENT,
    insurance_id int,
    mortgage_id int,
    property_name varchar(128),
    type varchar(128),
    address varchar(128),
    cost double,
    nber_spots int,
    rental_price double,
    property_tax_rate double,
    school_tax_rate double,
    condo_fee double,
	CONSTRAINT fk_insurance_id
    FOREIGN KEY (insurance_id) 
    REFERENCES insurance(insurance_id) ON DELETE CASCADE,
	CONSTRAINT fk_mortage_id
    FOREIGN KEY (mortgage_id) 
    REFERENCES mortgage(mortgage_id) ON DELETE CASCADE
);

CREATE TABLE contractor(
	contractor_id int PRIMARY KEY AUTO_INCREMENT,
    contractor_name varchar(128),
    company varchar(128),
    phone_number char(12),
    email varchar(128),
    profession varchar(128)
);

CREATE TABLE renovation(
	renovation_id int PRIMARY KEY AUTO_INCREMENT,
    contractor_id int, 
    property_id int,
    renovation_type varchar(128),
    description varchar(128),
    cost double,
    isFinished boolean DEFAULT false,
	CONSTRAINT fk_contractor_id
    FOREIGN KEY (contractor_id) 
    REFERENCES contractor(contractor_id) ON DELETE CASCADE,
	CONSTRAINT fk_property_renovation
    FOREIGN KEY (property_id) 
    REFERENCES property(property_id) ON DELETE CASCADE
);

CREATE TABLE tenant(
	tenant_id int PRIMARY KEY AUTO_INCREMENT,
    property_id int,
    fullname varchar(128),
    phone_number char(14),
    email varchar(128),
    rent_start_date date,
    rent_end_date date,
    yearly_income int,
    income_debt_ratio int,
    has_paid_rent boolean,
    payment_type varchar(128),
	CONSTRAINT fk_property_tenants
    FOREIGN KEY (property_id) 
    REFERENCES property(property_id) ON DELETE CASCADE
);

CREATE TABLE lease(
	lease_id int PRIMARY KEY AUTO_INCREMENT,
    tenant_id int,
    start_date date,
    end_date date,
    lease_path varchar(500),
	CONSTRAINT fk_tenant_id
    FOREIGN KEY (tenant_id) 
    REFERENCES tenant(tenant_id) ON DELETE CASCADE
);

delimiter //
CREATE TRIGGER after_delete_property
AFTER DELETE
ON property
FOR EACH ROW
BEGIN
	DELETE FROM mortgage where mortgage_id = OLD.mortgage_id;
    DELETE FROM tenant WHERE property_id = OLD.property_id;
    DELETE FROM renovation WHERE property_id = OLD.property_id;
END;
//

