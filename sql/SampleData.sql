/*Bank Institutions*/
INSERT INTO bank_institution (institution_name,interest_rate) VALUES ("RBC",2.49);
INSERT INTO bank_institution (institution_name,interest_rate) VALUES ("BMO",2.34);
INSERT INTO bank_institution (institution_name,interest_rate) VALUES ("CIBC",2.44);

/*Contractors*/
INSERT INTO contractor (contractor_name,company,phone_number,email,profession) VALUES ("Helen","Fix and Repairs","514-222-3333","helen@email.com","Paint");
INSERT INTO contractor (contractor_name,company,phone_number,email,profession) VALUES ("Tony","RoofRoof","514-111-3333","tony@email.com","Roof");
INSERT INTO contractor (contractor_name,company,phone_number,email,profession) VALUES ("Bob","Build","514-222-5555","bob@email.com","Plumbing");

/*Inusrance*/
INSERT INTO insurance (insurance_name,insurance_amount) VALUES("Intact",3000);
INSERT INTO insurance (insurance_name,insurance_amount) VALUES("La Capital",4000);
INSERT INTO insurance (insurance_name,insurance_amount) VALUES("DesJardins",5000);

/*Mortgages*/
INSERT INTO mortgage (bank_id,payment_date,start_date,end_date,down_payment) VALUES (1,"2021-04-30","2020-04-30","2025-4-30",5000);
INSERT INTO mortgage (bank_id,payment_date,start_date,end_date,down_payment) VALUES (3,"2021-06-28","2019-06-28","2024-06-28",2000);
INSERT INTO mortgage (bank_id,payment_date,start_date,end_date,down_payment) VALUES (2,"2021-04-24","2018-04-24","2023-04-24",500);

/*Porperties*/
INSERT INTO property (insurance_id,mortgage_id,property_name,type,address,cost,nber_spots,rental_price,property_tax_rate,school_tax_rate,condo_fee)
VALUES(1,1,"Greenspace","Condo","123 street, Montreal, Quebec",500000.00,1,10000,0.05,0.02,2);
INSERT INTO property (insurance_id,mortgage_id,property_name,type,address,cost,nber_spots,rental_price,property_tax_rate,school_tax_rate,condo_fee)
VALUES(2,2,"CityViews","Plex","123 blvd, Laval, Quebec",550000.00,6,10000,0.06,0.03,null);
INSERT INTO property (insurance_id,mortgage_id,property_name,type,address,cost,nber_spots,rental_price,property_tax_rate,school_tax_rate,condo_fee)
VALUES(3,3,"LakeSide","House","546 blvd, Brossard, Quebec",500000.00,1,10000,0.05,0.02,null);

/*Renovations*/
INSERT INTO renovation (contractor_id, property_id, renovation_type,description,cost,isFinished)
VALUES(1,1,"plumbing","Sink is leaking",200,false);
INSERT INTO renovation (contractor_id, property_id, renovation_type,description,cost,isFinished)
VALUES(2,3,"drywall","whole in the wall",300,true);
INSERT INTO renovation (contractor_id, property_id, renovation_type,description,cost,isFinished)
VALUES(3,2,"heating","broken heater",100,true);

/* Tenant */
INSERT INTO tenant (property_id,fullname,phone_number,email,rent_start_date,rent_end_date,yearly_income,income_debt_ratio,has_paid_rent,payment_type)
VALUES(1,"Alice Beaudoin","514-232-3949","alice@email.com","2021-04-30","2021-05-30",30000,1.2,true,"Direct Deposit");
INSERT INTO tenant (property_id,fullname,phone_number,email,rent_start_date,rent_end_date,yearly_income,income_debt_ratio,has_paid_rent,payment_type)
VALUES(2,"Bob Blake","514-293-2983","bob@email.com","2021-04-23","2021-05-23",40000,1.1,false,"Cheque");
INSERT INTO tenant (property_id,fullname,phone_number,email,rent_start_date,rent_end_date,yearly_income,income_debt_ratio,has_paid_rent,payment_type)
VALUES(2,"Charles Charlie","514-184-3847","charles@email.com","2021-04-23","2021-05-23",40000,1.1,true,"Cheque");

/*Lease*/
INSERT INTO lease (tenant_id,start_date,end_date,lease_path)
VALUES (1,"2021-04-30","2022-04-30","C:\\Users\\leduy\\courses\\java410\\DestinationLease\\Greenspace_Alice Beaudoin.pdf");
INSERT INTO lease (tenant_id,start_date,end_date,lease_path)
VALUES (2,"2021-04-23","2022-04-23","C:\\Users\\leduy\\courses\\java410\\DestinationLease\\CityViews_Bob Blake.pdf");
INSERT INTO lease (tenant_id,start_date,end_date,lease_path)
VALUES (3,"2021-04-23","2022-04-23","C:\\Users\\leduy\\courses\\java410\\DestinationLease\\CityViews_Charles Charlie.pdf");
